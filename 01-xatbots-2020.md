# Curset Xatbots

## BotFather @botFather

- per a crear comptes bot i gestionar els existents.

- Descripció: https://core.telegram.org/bots
- API manual: https://core.telegram.org/bots/api
- Suport: @BotSupport

---
~~~
BotFather, [10.05.20 14:13]
I can help you create and manage Telegram bots. If you're new to the Bot API, please see the manual (https://core.telegram.org/bots).

You can control me by sending these commands:

/newbot - create a new bot
/mybots - edit your bots [beta]

Edit Bots
/setname - change a bot's name
/setdescription - change bot description
/setabouttext - change bot about info
/setuserpic - change bot profile photo
/setcommands - change the list of commands
/deletebot - delete a bot

Bot Settings
/token - generate authorization token
/revoke - revoke bot access token
/setinline - toggle inline mode (https://core.telegram.org/bots/inline)
/setinlinegeo - toggle inline location requests (https://core.telegram.org/bots/inline#location-based-results)
/setinlinefeedback - change inline feedback (https://core.telegram.org/bots/inline#collecting-feedback) settings
/setjoingroups - can your bot be added to groups?
/setprivacy - toggle privacy mode (https://core.telegram.org/bots#privacy-mode) in groups

Games
/mygames - edit your games (https://core.telegram.org/bots/games) [beta]
/newgame - create a new game (https://core.telegram.org/bots/games)
/listgames - get a list of your games
/editgame - edit a game
/deletegame - delete an existing game
~~~
---

# Què és Telegram

- https://www.youtube.com/watch?v=wnyML5TqT_4&t=403s

Ara ja passarem a parlar de que és un bot o xatbot quan l'apliquem a la comunicació. A la Vikipèdia podem llegir : " Els bots conversacionals són anomenats col·loquialment "xatbots". El terme “xatbot“ és el resultat de la suma dels mots Xat (de l'anglès chat, "xerrar") i “Bot” (contracció del terme Robot, programa informàtic que imita el comportament d'una persona). Va ser emprat per primera vegada per Michael Mauldin al 1994 en el seu article "ChatterBots, TinyMuds, and the Turing Test: Entering the Loebner Prize Competition".

La definició clàssica de xatbot (o bot conversacional) és un programa d’intel.ligència artificial  que processa missatges en llenguatge natural emesos per l’usuari a través de sistemes de teclat o audio i que genera respostes que són enviades de nou a l’usuari, amb l’objectiu de simular una conversa real entre dos humans.

També se’ls denomina «assistents virtuals» o «assistents personals» quan permeten una interacció detallada i atenta a les necessitats particulars de cada usuari. Quan l’objectiu final d’aquests programes és donar resposta a dubtes i/o sol.licituds respecte a un tema específic sense necessitat que existeixi un intermediari humà es denominen “assistents virtuals”."

Per tant ens mourem en els conceptes : bot, xatbot i assistent virtual, en  funció de com interactuï el programa creat.

Per agafar un visió general dels xatbots visioneu aquest vídeo : https://www.youtube.com/watch?v=Aydn438OSWA

## Bots preparats

- Vídeo: https://www.youtube.com/watch?v=Aydn438OSWA
- Millors bots de telegram: https://reydefine.com/mejores-bots-telegram/
- Automatització amb ITTF: https://reydefine.com/ifttt-automatizacion/

- Fer cerca al camp de cerca (es pot posar el nom del bot directament al camp per escriure missatges, però no tots van així)

- per iniciar executar /start

- @apkmanagerbot - Per instal·lar apks

### imatges, memes, etc
- @gif invoca el bot de gifs
- @sticker – Find and send stickers based on emoji
- @pic – Yandex image search
- @texttogifbot - Crea gifs amb el text en moviment
- @memerator_bot - Crea memes amb fotos

### Compres
- @amazonglobalbot logitech mouse per mirar preus (aquí ratolins) d'amazon

### Traducció i conversió
- @ytranslatebot traducció d'idiomes
- @TextTSBot - Llegeix texti he afegit
- @voicybot - Passa veu a text
- @pdfbot . Converteix i fa operacions amb pdf
- @qrqrbot - Converteix un text qualsevol a QR.

### Vídeo, música
- @imdb - IMDB
- @iLyricsBot to search for Lyrics to any song
- @vkmusicbot - Buscar música a internet
- @youtube - Connect your account for personalized results
- @music - Search and send classical music
- @vid - per a veure vídeos
- @ts2chboot - Converteix fitxer de vídeo a vídeo streaming

### Alertes, organització
- @alertbot - Crea alertes temporitzades
- @todotask_bot - Gestor de tasques a fer

### Captcha
- @join_captcha_bot cal afegir-lo com a membre del grup i donar-li permís d'admin. Controla l'entrada de membres.

### Cercadors
- @bing – Bing image search
- @wiki – Wikipedia search

### Jocs
- @gamee - Jocs al telegram
- @triviaBot Preguntes del trivial

### Mapes
- @foursquare – Find and send venue addresses

### Enquestes
- @pollbot - Per a crear enquestes

### Format de text
- @bold – Make bold, italic or fixed sys text

### Antivirus
- @drwebbot - Antivirus
- @virustotalurlbot - Un altre


- Més bots: https://telegram-store.com/

---
## Núvol de Telegram

- Es pot crear canals privats per a posar compartit allà el que vulguem (fotos, pelis, enllaços, etc).
- Qualsevol grup o canal en propietats te pestanyes per organitzar el contingut en diferents categories: Enllaços, víedoes, imatges, etc.
- Permet fitxers de fins a 1,5 GB i es pot enviar el que vulguem a aquest grup o canal per que quedi guardat.  

---
## Creació del bot

1. Buscar botfather per crear el bot.
2. Posar nom al bot i nom d'usuari (que hauria d'acaba en "\_bot"

~~~
Done! Congratulations on your new bot. You will find it at t.me/ptome_bot. You can now add a description, about section and profile picture for your bot, see /help for a list of commands. By the way, when you've finished creating your cool bot, ping our Bot Support if you want a better username for it. Just make sure the bot is fully operational before you do this.

Use this token to access the HTTP API:
1240462513:AAEx_Cr1qRBlmPMtNY9iz9FdDOGGz-7GJ20
Keep your token secure and store it safely, it can be used by anyone to control your bot.

For a description of the Bot API, see this page: https://core.telegram.org/bots/api
~~~

---
## Telegram bot with google apps scripts

- https://www.youtube.com/playlist?list=PLGGHwNnXfci86dfqIVLc5l391SPk-RX1F

---
