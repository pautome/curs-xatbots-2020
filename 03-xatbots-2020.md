# Entrega del Projecte 3

- En aquesta unitat haureu d'anar enganxat les evidencies en una plantilla de presentació de Google Slides, que tindreu ja pautada per simplificar la tasca, cliqueu aquí per accedir a la plantilla: https://drive.google.com/open?id=1raewRQRj9mb9D-CvWQkCc2-jc4VWqDeVqG8sjHzvXIQ

- Repassa les evidencies de les diferents activitats que contempla aquest Tema 3 :

1. Activitat 3.1 Dues captures d'aplicació entre les 10 FAQS
2. Activitat 3.2  La incorporació al Xatbot d'un webservice
3. Activitat 3.3 La incorporació d'un registre log de tots els missatges que arriben al grup i La incorporació d'un qüestionari de preguntes auto-avaluables pels alumnes
4. Activitat 3.4 La definició de perfils , l'accés a una carpeta del Drive i la consulta privada de notes
5. Activitat 3.5 La implementació a les Jornades de la recollida d'imatges i la validació de l'entrada QR

## FAQS per a principiants amb Telegram

1. Es pot crear una App amb Google Apps Script sense emprar el full de càlcul ?  [0:13]
   - Si es pot fer però perdem la possibilitat de guardar o accedir a dades que es guarden al full de càlcul.
   - Anem al menú de Drive i fem nuevo->google apps script per a crear un script desvinculat de full de càlcul.
2. Com ho podem fer per detectar els errors que impedeixen que funcioni el xatbot ? [1:38]
  1. Crear funcions que simulin la crida des de fora
  ~~~
  function testCertificat(){
    certificat("379302885","ca");
  }
  function testIden(){
    iden("379302885","","ca");
  }
  function testEscriure_frase(){
    escriu_frase("379302885","Prova");
  }
  ~~~

  - Depurar amb debugger: Picant a l'esquerra de l'editor de codi podem posar breakpoint
  - Escollir funció de test al desplegable i picant el "bitxo" activem depurador.

    ![](imgs/02-xatbots-2020-79f75c8c.png)

  2. Enviar missatges de log quan passa alguna cosa. Després veure amb "mostra->registres". https://developers.google.com/apps-script/guides/logging#basic_logging
  ~~~
  Logger.log('Emailing data row ' + rowNumber + ' to ' + email);
  ~~~

   1. Enviar missatges de depuració al bot (són prints al bot amb missatges quan passem per un lloc del codi):

   ~~~
   id_debug=id_usuari;
   ...
   function debug(text){
     sendText(id_debug,"DEBUG: "+ text);
   }
   ~~~

3. Com podem veure els paràmetres que Telegram envia al Xatbot ?  [8:20]

  - Quan fem un inline al xatbot o bé enviem documents al Bot, podem analitzar el que Telegrama envia al bot de diverses formes. Una pot ser amb un correu, utilitzant la funció:
  ~~~
  MailApp.sendEmail("correu@gmail.com","JSON TELEGRAM",JSON.stringify(data,null,4) );
  ~~~
  - https://developers.google.com/apps-script/reference/mail/mail-app


4. Com podem afegir funcions de tramesa de continguts a Telegram ? [13:17]

   - Al codi del xatbot tenim exemples de codi per enviar objectes al nostre xatbot des d'un script de Google Apps.
   - Per exemple per enviar audio:
     - https://core.telegram.org/bots/api#audio
     - https://rdrr.io/cran/telegram.bot/
     - https://rdrr.io/cran/telegram.bot/man/sendAudio.html

5. Existeix algun assistent de programació per a Google Apps Script ? [19:29]

- A mida que es fan servir funcions de Google Apps Scripts, apareix un desplegable amb els mètodes de la classe. Per exemple:

![](imgs/03-xatbots-2020-dd0a9e5a.png)

6. Com podem conèixer l'id del grup o canal per enviar-hi informació des del xatbot ? [22:11]
https://apps.timwhitlock.info/emoji/tables/unicode
- A la versió web de telegram podem veure l'identificador de grup o canal al a URL. Li treiem la g de davant i ens quedem amb l'identificador al que cal posar un guió "-" davant.
- Si és un canal, a l'identificador li afegim davant ""-100".

7. Podem accedir a informació externa des del xatbot ? [26:23]

- Es pot accedir a fonts externes, per exemple pastebin. Cal afegir la URL del fitxer que tenim a pastebin, i fent ús de la funció UrlFetch obtenim el contingut de la pàgina.

~~~
...
response=UrlFetch(url);
sendtext(id, escriu_frase(response,idioma));
...
~~~


8. Com ho hem de fer per utilitzar serveis de Google ? [31:49]

- Ens demana permís per a fer servir l'usuari automàticament en publicar l'aplicació.
- Altres vegades no és automàtic i cal anar a Recusos->Servicios avanzados de Google. Si en passa que no va, mirem d'activar-ho.

9. Quins tipus de botons podem fer servir amb els grups i canals de Telegram ? [34:37]

- Hi ha dos tipus de botons: dintre de la finestra de chat o sota el quadre de text on podem escriure.
  - Inline keyboard: Dintre el chat. En picar es pot enviar un callback query i una URL.
  - Keyboard: Sota el quadre de text, menys flexible.

10. Com es fa per enviar icones dins dels botons? [37:58]

- Hi ha unes taules de emojis Unicode, en definitiva text, que podem fer servir com icones dels botons. https://apps.timwhitlock.info/emoji/tables/unicode

![](imgs/03-xatbots-2020-8503051f.png)

- Piquem a sobre el codi, s'obre una nova pàgina amb la icona-caràcter. Copiem i enganxem el símbol directament de la pàgina web al codi del XATBOT.
