# Projectes del curs

## Projecte 1

Aquí pots veure la Rubrica que aplicaré en aquest Primer Projecte, pensant en simplificar les validacions del  nostre curs, i d'altra banda en donar-vos idees per aplicar rubriques amb els vostres alumne

Una vegada realitzades totes les activitats proposades per fer l'entrega d'aquest projecte fes el següent :

Grava en veu dins del teu grup de Telegram , una explicació repassant activitat per activitat el que has fet, com i identificant els noms dels bots testejats i dels membres afegits al grup. Repassa les activitats :


1.1.-  Introducció als bots . Has creat el grup i el canal amb els membres afegits?
- He creat el grup @grup_ptome i el canal @canal_pau_tome. Després he compartit l'enllaç a tots dos i s'han afegit alguns companys.

1.2 .- Registre del primer bot . Has registrat el teu bot?
- He executat el @botfather i creat el meu @ptome_bot. L'he personalitzat amb logo i descripció. He posat algunes comandes.

1.3 .- Bots inline . Quins bots inline has testejat que t'haguin agradat mes i quins son els 5 escollits?

He provat:
- @gif invoca el bot de gifs
- @sticker – Find and send stickers based on emoji
- @pic – Yandex image search
- @wiki – Wikipedia search
- @iLyricsBot to search for Lyrics to any song

1.4 .- Bots públics .-Quins 10 bots públics has escollit ?

1. @ytranslatebot traducció d'idiomes
2. @TextTSBot - Llegeix text
3. @voicybot - Passa veu a text
4. @pdfbot . Converteix i fa operacions amb pdf
5. @qrqrbot - Converteix un text qualsevol a QR.
6. @alertbot - Crea alertes temporitzades
7. @todotask_bot - Gestor de tasques a fer
8. @join_captcha_bot cal afegir-lo com a membre del grup i donar-li permís d'admin. Controla l'entrada de membres.
9. @gamee - Jocs al telegram
10. @triviaBot Preguntes del trivial

1.5 .-IFTTT i Telegram .- Quin RSS has afegit ?

- He creat el compte IFTTT i l'he enllaçat amb el meu grup. He fet que quan algú em segueix a twiter m'envii un missatge. També quan ha de ploure.

Una vegada tinguis la gravació aplica un conversor de veu a text per tenir ren lletra el que has explicat . Penja a la carpeta compartida amb el nom del teu grup els dos fitxers el de text amb extensió txt o altre de text i el d'àudio amb mmp3 o altre format d'àudio i una vegada pujats a la carpeta compartida envia la seva url aquí a la tasca del curs

![](imgs/exercicis-bf35a350.png)

---

## Projecte 2: Implementació de la gestió d'una Jornada docent amb Telegram

Per la seva característica de que tothom ha assistit a alguna Jornada Docent o n'ha estat organitzador, he escollit aquest tema per realitzar la primera implementació d'un xatbot de Telegram.

En primer lloc posar de rellevància que Telegram és només la plataforma vehicular, però que caldrà treballar molts aspectes fora del  propi Telegram. Per ser formalment correctes, en premer lloc hauríem de fer un anàlisi del projecte que volem estructurar, i una vegada fet l'anàlisi i la prospecció de mercat, passaríem a desenvolupar l'aplicació. Per això com us he dit he escollir un tema conegut per tothom per poder simplificar aquest aspecte d'anàlisi.

Per organitzar una Jornada Docent, podríem dir que la nostra aplicació ha de gestionar :

- Les inscripcions dels participants als Tallers
- L'elaboració de les llistes d'inscrits a cada Taller
- El control d'assistència dels participants a cada taller
- La informació a la xarxa pels assistents
- La impressió dels certificats d'assistència
- Com validar les evidències del treball fet?

- A cada Tema us aniré proposant una manera diferent de presentar les evidències amb l'objectiu de que  incrementeu el vostre banc de recursos en aquest àmbit. Així com en la presentació vareu fer un collage i en el tema 1 vareu incorporar el vostre treball directament al grup, ara el tema requereix que us quedi constància de codi i de dades informatives, per això la proposta serà que des del principi creeu un document o plantilla en Google Docs a la carpeta compartida, i aneu afegint les evidencies i altra informació al document amb Google Docs, de manera que de cara a un futur quan recupereu el document per aplicar el projecte amb els vostres alumnes, pugueu aprofitar tot allò que hi hagueu aportat.

- En aquest projecte 2 heu de crear un Document amb Google Docs i afegir-hi tot allò que feu o que creieu us pot interessar emprar en un futur .
---

- En aquesta unitat haureu d'entregat el document de Google que heu anat elaborant al llarg de les 3 activitats proposades . Pengeu el document a la carpeta compartida i deixeu la url en aquesta activitat
- Aquí pots veure la Rubrica que aplicaré en aquesta Unitat 2 , pensant en simplificar les validacions del nostre curs, i d'altra banda en donar-vos idees per aplicar rubriques amb els vostres alumne

- Una vegada realitzades totes les activitats proposades per fer l'entrega d'aquest projecte fes el següent :

- Repassa les evidencies que ha de contemplar el document de Google :

2.1.- El formulari per recollir les inscripcions, el full amb les inscripcions,el codi per enviar missatge i el missatge rebut en fer la inscripció

2.2 .- La plantilla de certificat creada, el llistat amb els 50-70 inscrits, el codi emprat per generar la combinació i el document amb els certificats resultants

2.3.- El codi complert del xatbot , el codi de les implementacions personals, les captures de les implementacions realitzades

- Una vegada tinguis el document amb totes les evidencies el penges a la carpeta compartida i enganxes la url en aquesta tasca del curs.

![](imgs/exercicis-112d8f12.png)

---
## Projecte 3: Projecte 3 : Els botons de l'aula

En aquest projecte tres ja entrarem de ple en aspectes docents, plantejant recursos que tenen molta relació amb la interacció docent en una aula. El títol "Els botons de l'aula" , te relació amb el gran paper que tenen els botons per ajudar-nos en la gestió dels xatbots de Telegram. Veurem botons per quasi tot !!!!! .

Els botons poden ser de dos tipus :

- keyboard : Son els que es situen per sota de la barra de missatges
- inline_keyboard : Son els que es situen dins de la finestra i es van desplaçant amb els missatges . Aquests botons inline_keyboard poden emprar diversos recursos per gestionar la seva elecció , el smés destacats son :
  - "callback_data" permet executar una comanda en clicar el botó
  - "url" executa directament una url accedint a un navegador web.

- Exemple dels dos tipus de botons
![](imgs/exercicis-285bcc95.png)

- Paginant noticies d'ofertes de treball procedents d'un RSS
![](imgs/exercicis-4c092d66.png)

- En aquest projecte es tractarà de que creeu una hipotètica gestió de grup classe a Telegram. L'element vehicular per entregar les evidencies, serà una presentació que us haureu de descarregar a la vostra carpeta compartida i que ja te les diapositives fetes perquè vosaltres aneu emplenant amb imatges i informació.

https://docs.google.com/presentation/d/1SGMPbO7q31GCOY_VZxjLQ6PS2PXRHdezSbsxI09B08I/edit#slide=id.g875ab01831_2_512

Els elements que haureu de gestionar i incorporar al vostre projecte són

1. Utilització del grup i canal creats en el projecte 1
2. Utilització de companys com alumnes del curs amb un delegat que tindrà un perfil especial que accedirà a les convocatòries: Haureu també de fer d'alumne d'almenys dos grups de companys
3. El xatbot tindrà un registre de tots els missatges enviats al grup
4. El xatbot tindrà un menú keyboard amb pefils diferents
5. Incorporareu al xatbot l'accés a un webservice
6. Els alumnes podran fer un qüestionari/test i veure privadament els seus resultats
7. A l'aula hi haurà accés a la carpeta de materials del Drive
8. Finalment incorporarem noves prestacions al xatbot de la Jornada Docent :
   1. La recollida d'imatges de la Jornada que se enviaran al grup i es gravaran en una carpeta del drive
   2. La validació de l'entrada amb codi QR

- Aquesta unitat està enfocada en gran part a fer molt de copiar/enganxar perquè aneu testejant recursos i veure que us poden aportar i treure idees de cara a possibles utilitzacions amb altres formats i prestacions diferents a com han estat plantejades en el curs.

---

### 3.1.- FAQS per principants amb Telegram

- Crec que plantejar una activitat de FAQS al començament de conèixer com treballen els xatbots , no es gaire efectiva, per això ara que ja heu treballat una mica tant amb la plataforma com amb el codi, crec que us serà molt més profitós. He recopilat un conjunt de 10 qüestions que solen ser molt genèriques entre els principiants, podeu optar per seguir tot el vídeo o anar directament a les que us siguin de més interès.


- En aquest activitat trobareu a la presentació que us demano enganxar 2 captures relacionades amb l'aplicació dels FAQS i que anoteu una possible FAQS per altres temes.

- https://youtu.be/ToWOrPM81Cg

1. Es pot crear una App amb Google Apps Script sense emprar el full de càlcul ?  [0:13]
2. Com ho podem fer per detectar els errors que impedeixen que funcioni el xatbot ? [1:38]
3. Com podem veure el paràmetres que Telegram envia al Xatbot ?  [8:20]
4. Com podem afegir funcions de tramesa de continguts a Telegram ? [13:17]
5. Existeix algun assistent de programació per a Google Apps Script ? [19:29]
6. Com podem conèixer l'id del grup o canal per enviar-hi informació des del xatbot ? [22:11]
7. Podem accedir a informació externa des del xatbot ? [26:23]
8. Com ho hem de fer per utilitzar serveis de Google ? [31:49]
9. Quins tipus de botons podem fer servir amb els grups i canals de Telegram ? [34:37]
10. Com es fa per enviar icones dins dels botons? [37:58]

### 3.2.- Acces a dades externes via API webservices

- Per Internet tenim accés a una gran diversitat de serveis de dades via webservices, que significa que de manera automatitzada emprant programes informàtics podem accedir a dades facilitades per altres proveïdors . El principal problema es que molts serveis rellevants son de pagament, malgrat això existeixen iniciatives de dades obertes que promouen posar a disposició dels usuaris  tot tipus de dada d'interès públic.  Aquí teniu dos d'aquests  projectes:

* Dades Obertes de la Generalitat de Catalunya
Linkind Data Open Cloud (LDOC)
En aquesta activitat intentarem veure com podem incorporar dades de webservices externs al nostre grup o canal emprant un xatbot . Per l'intercanvi d'informació habitualment es fan servir diversos formats de dades, entre ells els més utilitzats son JSON i XML, que veurem exemples de cada tipus.

* En aquesta activitat haureu d'incorporar 1 webservices al vostre xatbot perque mostin la informació al vostre canal. Partireu del codi que teniu sobre les Jornades ( fent copiar/enganxar i adaptant les dades per aquest segon xatbot)  i anireu construint el xatbot de l'aula

* Podeu escollir entre un dels dos tipus de dades JSON o XML
Podeu incorporar RSS com les noticies del vostre centre des del nodes o la proposta de feina que teniu de mostra per a la vostra tutoria
Pot ser de la Plataforma de dades obertes de la Generalitat
Accés a dades exernes amb format JSON
En aquesta activitat accedirem a la informació meteorològica, de manera que llegirem la informació en format JSON  d'un proveïdor  de dades meteorològiques, i les mostrarem al nostre canal cada dia entre les 6 i les 7 del matí

Utilitzar el servei Mundial Openweathermap
Aquest servei te l'inconvenient de que cal registrar-se ( gratuïtament) per obtenir el codi API_KEY necessari per obtenir les dades, però l'avantatge de que dona molta informació, de manera senzilla i molt variada a més de facilitar les imatges representatives del temps .

Procés per aplicar aquest webservice :

  1.  Donar-nos d'alta a la web de openweathermap https://home.openweathermap.org/users/sign_up i obtenir el codi.
  2. Localitzar les coordenades del nostre centre, per exemple via Google Maps
    1. Escrivim a Google Maps elnom o l'adreça del nostre centre i en fem la localització al mapa
    2. Cliquem el botó dret del ratolí sobre el blobus del nostre centre
    3. S'obrirà un menú i escollirem l'opció "Que hi ha quí ?
    4. Ens mostrarà en la part inferior les dues coordenades la latitud i la longitud ,en el nostre cas (41.398178, 2.139626 )
  3. Testejarem si accedim correctament a les dades i en visualitzarem el format per poder-ne fer l'extracció de les mateixes , per això aplicarem la sintaxi següent :
http://api.openweathermap.org/data/2.5/weather?lat=41.398178&lon=2.139626&APPID=API_KEY&lang=ca&units=metric

![](imgs/exercicis-eaa8a8b3.png)

  4. Entrarem al nostre codi del xatbot i crearem la funció per llegir aquestes dades:

~~~
function meteo_canal()
{
meteo(ID_canal,"ca");   
}


function meteo(id,idioma)
{
 var coor_lat = "";  // la latitud del nostre institut

 var coor_long = "";  // La longitud del nostre Institut


  var API_KEY = " " ; // API Key que obtindrem al registrarnos a https://home.openweathermap.org/users/sign_up     
  var url_meteo = "http://api.openweathermap.org/data/2.5/weather?lat=" + coor_lat + "&lon=" + coor_long + "&APPID=" +API_KEY + "&units=metric&lang="+idioma;


  var response_meteo = UrlFetchApp.fetch(url_meteo);   // Anirem a la web de meteo i recuperarem la informació a la variable response_meteo
  var json = response_meteo.getContentText();       //  Convertim el resultat obtingut que serà un fitxer en format JSON a una variable de text
  var dades = JSON.parse(json);       //Parsejem, que consistirà en convertir el resultat a files i columnes

  Logger.log(dades); //Fem un log per veure el resultat si es produís alguna errada

  // Per recuperar les dades haurem de fer servir els noms i les rutes que podem veure en el quadre anterior  

 // Llegim les dades que ens interessen

  var lloc = dades.name;
  var temperatura = dades.main.temp;
  var pressio = dades.main.pressure;
  var humitat = dades.main.humidity;


  var icona = dades.weather[0].icon;  // obtenim el nom de la icona representativa del temps
  var general = dades.weather[0].description;

 // Construim la sortida de dades al nostre canal

  var resposta_meteo = "[" + lloc + "]  %0A" ; // els salts de linia en un caption es fan amb "%0A"
  resposta_meteo = resposta_meteo + "Temperatura actual = " + temperatura + " ºC %0A" ;
  resposta_meteo = resposta_meteo + "Humitat = " + humitat + "%25" + "  %0A" ;
  resposta_meteo = resposta_meteo + "Pressió =  " + pressio + " mb  %0A" ;
  resposta_meteo = resposta_meteo + "Temps general = " + general + " %0A" ;  

  var icona_meteo = "http://openweathermap.org/img/w/" + icona + ".png" ;  // Construim la ruta del fitxer amb la imatge del temps

//Logger.log(response_meteo.getContentText()); //  Podem afegir un  logger per veure si funciona bé

sendPhoto(ID_canal,icona_meteo, resposta_meteo);

}
~~~

  5. Quan ja tinguem el codi de la funció l'hem de implementar dins del xatbot de dues maneres :
    - Creant un activador perque l'executi entre 6 i 7 del matí per mostrar al canal la meteorologia prevista pel dia ( Veure activitat 2.1 )
    - Creant una comanda del menú que executi la funció però que la mostri en privat .

- Perquè mostri la meteo en la finestra privada, per la comanda "/meteo" aplicarem meteo(id_usuari,idioma) , i per l'activador com que no podem passar la id, crearem una funció meteo_canal que li pasarem la id del canal a la funció meteo:  

~~~
function meteo_canal()

{

meteo(ID_canal,"ca");

}
~~~

- El resultat que veurem serà com aquest :

![](imgs/exercicis-354c70e2.png)

- Accés a dades exernes amb format RSS
El format RSS es un model del tipus XML que te unes determinades etiquetes definides amb un nom i un tipus de dada concrets. El format RSS es fa servir principalment per tractar informació de tipus periodístics, i també es fa servir per intercanviar informació en els blocs de noticies. Donat que estem treballant l'aula de FP  i una opció serà la tutoria, he escollit com a exemple un RSS del Portal Feina Activa del Catàleg de dades Obertes de la Generalitat de Catalunya . Podem escollir per àmbits laborals, he escollit el de Sanitat i Salut :

1.- Escollirem l'àmbit del RSS que afecta als nostres alumnes : https://feinaactiva.gencat.cat/web/guest/home/-/news_rss/rss/catalunya/sanitat_salut

![](imgs/exercicis-dbfea3db.png)

2.- Com el format RSS ja esta definit serà més fàcil localitzar la informació que volem. Si cliquem el botó dret i visualitzem el codi veurem l'estructura del fitxer :

![](imgs/exercicis-1c185802.png)

3.- Desenvoluparem el codi que hi afegirem un element especial, la paginació de botons ..... . Com que pel que hem pogut veure el RSS d'ofertes sol tenir 50 noticies, inclourem una paginació que la podeu emprar pel altres situacions . Visualitzarem les ofertes amb un menú de botons i paginades de 10 en deu :

![](imgs/exercicis-4c092d66.png)

Aquí teniu el codi per incloure al vostre xatbot :

~~~
function ofertes_treball(id_usuari,text,idioma)
{

var num = text.split(' ');   // Recupera text per conèixer la posició inicial
var inici = parseFloat(num[1]) ; // Agafa la dreta del text que contindrà o res o un numero de posició inicial
var inici = inici || 0;  // Si no hi ha valor posa 0 per començar
var i = inici;   // Actualitza la variable "i" inicial  per fer el bucle de noticies
var fi_pagina = parseFloat(inici)+9; // converteix el valor entrat a numero i suma 9 per tenir el final de la pagina


//https://analisi.transparenciacatalunya.cat/Treball/Feina-activa-Ofertes-de-feina-per-sectors/k4kj-stq9 // Portal de Feina

  var url ="https://feinaactiva.gencat.cat/web/guest/home/-/news_rss/rss/catalunya/sanitat_salut";  // Url del RSS de sanitat
  var feed = UrlFetchApp.fetch(url).getContentText(); // Carreguem les dades a la variable feed


  var doc = XmlService.parse(feed); // Parsegem el RSS en estructura de files i columnes
  var root = doc.getRootElement();  // Tot RSS te una etiqueta arrel de començament que en el RSS es justament <rss>
  var channel = root.getChild('channel'); //Carrega les dades de l'etiqueta <channel> que no farem servir
  var items = channel.getChildren('item'); // Carrega totes les etiquetes <item> normalment en te 50 en una llista

  var num_max = items.length - 1;  // per conèixer la quantitat de noticies que tenim

  var llista = new Array();    // Iniciem la llista per recollir la informació


  while (i <= fi_pagina  && i <= num_max ) {  // Anirem fent el bucle per cada noticia <item> mentre no estiguem al final de pagina o al final de totes les noticies  
    var item = items[i++]; // Va carregant cada noticia de la llista items i aprofita per incrmenentar el comptador "i"
  var titol = item.getChildText('title'); // Recuperem el títol de la noticia que toca
  var url = item.getChildText('link'); // Recuperem l'enllaç a la noticia
  var author = item.getChildText('author'); // Recuperem l'autor
  var dia = item.getChildText('pubDate'); // Recuperem la Data
  var descri = item.getChildText('description'); // Recuperem la descripció

   llista.push([{'text': i + "-" + titol, 'url': url  }]); // Afegim la noticia a la llista , incloim el títol i la url
    }

  //  Afegim el botons finals de paginació

  if(i<11) // Si estem a la primera pagina no podem tornar endarrere

  {
     llista.push([{'text': " Ofertes >>>" , "callback_data" : "/ofertes_treball 10"} ]);
  }

  if(i>11 && i< num_max)// Si estem a la segona pagina i no ham eacabat les noticies afegirem endavant i endarrere
  {
     llista.push([{'text': "<<< Ofertes" , "callback_data" : "/ofertes_treball "+ (i-20) },
                  {'text': "Ofertes >>>" , "callback_data" : "/ofertes_treball "+ (i)} ]);
  }

  if(i>= num_max) // si hem arribat al final no podem avançar pagina
  {
     llista.push([{'text': "<<< Ofertes" , "callback_data" : "/ofertes_treball "+ (i-20)} ]);
  }



   var tecles =  { inline_keyboard: llista    , resize_keyboard: true,one_time_keyboard : true  };
          sendText(id_usuari, "Ofertes de Treball " , tecles );  
  return true;

  }
~~~

- En aquesta activitat heu d'incloure un  serveisweb al vostre projecte que tingui doble activació per comanda des del grup o xatbot i amb un activador a una hora o donada una situació

- Una vegada realitzat heu d'emplenar la diapositiva corresponent del projecte amb una captura de com la mostra el xatbit i el codi utilitzat

---

### 3.3 .- Gravar i llegir dades des del full de càlcul

#### Gravar i llegir dades des del full de càlcul

- Com ja he indicat en diverses ocasions Google ens ofereix moltes facilitats per accedir a la informació emmagatzemada amb les seves plataformes amb unes poques línies de codi podrem gravar o podrem llegir dades. és evident que estem parlant d'un volum de dades baix que és el que podem gestionar en un entorn educatiu, per tant tampoc hem de pensar en una gran plataforma de dades com Oracle , perquè amb les fulles de càlcul de Google en podrem tenir suficient pel volum normal en els centres educatius.

- Les comandes bàsiques per accedir a les dades del full de càlcul de Google, amb Google Apps Script pels nostres xatbots son:

~~~
function validar(id_usuari,titol,idioma)
{

  var sh = SpreadsheetApp.openById(ssId).getSheetByName("Alumnes") ; // Accedim 1r a la fulla de càlcul i després al full que te per nom "Alumnes"
  var dades = sh.getDataRange().getValues();  // Carreguen totes les file si columnes a una variable que anomenem dades


   for(i in dades)    // fem un bucle recorrent totes les files de dadesfent servir i de comptador que comença per 0
  {
    var row = dades[i];  //Carreguem en cada pas la fila i de dades   
    var data = row[0];   // Carreguem els camps que es comencen a comptar per l'index 0
    var titol0  = row[1];
    var num_id = row[2];

    If(num_id == id_usuari) // comparatiu per detectar un usuari amb el codi que cerquem
    {
      sh.getRange(+i+1,5).setValue("Validat"); // Gravem el valor validat en el full de càlcul,en la fila "i" però hem de sumar 1, perquè comença per 1, i a la columna 6 que tindrà índex 5         
    }

  }  // fi del bucle for     

      sh.appendRow([new Date(),titol,id,idioma]); // Si no ha trobat l'usuari, Afegim un registre al final omplint amb els valors indicats seguin l'ordre de columnes
  }
~~~  

- En aquesta activitat te dos propostes, que hauràs de reflectir en les evidencies de la presentació :

- Fes una evidencia del log del teu grup
- Fes una evidencia del qüestionari que has creat i que ha de ser contestat per 2 companys , ala vegada que tu has de contestar el de dos fets per altres companys no sal que siguin els mateixos


#### Registrar en un log tots els missatges entren al xatbot

Podem tenir un registre en el nostre full de càlcul de tot el que escriure els usuaris del nostre grup, fins i tot podríem afegir un censurador de missatges en funció de paraules no permeses .

El procés per tenir aquest registre serà el següent :

Afegir un full que anomenarem "Log" .
En el Moment de fer la identificació de l'usuari li gravarem el text del seu missatge
Col.locarem la instrucció abans del switch que selecciona accions .
Farem servir una única instrucció:   SpreadsheetApp.openById(ssId).getSheetByName("Log").appendRow([new Date(),id_usuari,text,idioma]);

![](imgs/exercicis-04edc760.png)

Podem veure com grava el registre de missatges :

![](imgs/exercicis-04edc760.png)

1. Indicarem un full independent per gravar el registre
2. Ha quedat en blanc perquè el usuari ha enviat un tipus d'element no controlat per exemple un àudio
3. Quan es fa start no agafa el idioma i això després pot donar problemes si fem servir aquest pàrametres

### Crear un questionari/test per fer preguntes

- En una aula no podien faltar els exàmens, amb Telegram aprofitant la seva plataforma podem construir un senzill però eficient i personalitzat sistema de fer qüestionaris automatitzats i autocorregibles. El més important és dissenyar el sistema que volem utilitzar per fer aquest TEST. Podem pensar moltíssims dissenys i estratègies diferents,  La que us proposo consisteix en :

1. Crear un full per contenir les preguntes , que anomenarem "Preguntes" a més del full "Alumnes" amb el registre dels alumnes del curs.
![](imgs/exercicis-9a39f402.png)

2. Omplirem les preguntes en aquest full seguint la següent estructura :
  Columna A = Enunciat de la pregunta
  Columna B = Primera proposta de resposta
  Columna C = Segona proposta de resposta
  Columna D = Tercera proposta de resposta
  Columna E = Numero de la resposta correcte

3. En el full d'Alumnes afegirem 3 columnes :
  - Columna 1 = Per gravar les preguntes que li van sortint a l'atzar separades per "-"
  - Columna 2 = Per gravar si ha fet encert (19 o errada (0) en les respostes
  - Columna 3 = Resultat en el format 3/5 . Encerts / preguntes totals

![](imgs/exercicis-0b6c8b14.png)

4. Limitarem a 5 preguntes , arribat a contestar 5 preguntes ja no li sortiran més
5. En accedir a la funció pregunta comprobarà que no hagui arribat ja a les 5 preguntes contestades., i si és així enviarà un missatge informatiu sense seguir el procés.
6. Si segueix  la comanda escollira una pregunta a l'atzar entre totes. La afegirà a la llista de preguntes de l'úsuari/a i la mostrarà de la següent manera :
  1. L'enunciat de la pregunta com a text
  2. Les 3 possibles respostes aquests botons porten com a "callback_data" : "/res 1-5" que voldrà dir que si cliquem el botó executarà la comanda  /res que analitza les

![](imgs/exercicis-1c05983c.png)

Si anem fent mes preguntes arribarem a la 5 :

![](imgs/exercicis-2d418452.png)


Aquí teniu el codi de pregunta i resultat :

~~~
function pregunta(id_usuari)
{

  var sh = SpreadsheetApp.openById(ssId).getSheetByName("Alumnes") ; // Accedim 1r a la fulla de càlcul i després al full que te per nom "Alumnes"
  var dades = sh.getDataRange().getValues();  // Carreguen totes les file si columnes a una variable que anomenem dades

    for(i in dades)     // Localitzem l'usuari per recuperar les preguntes que ja ha fet
  {
    var row = dades[i];
    var num_usuari = row[14];

    if(num_usuari == id_usuari)  // Identifiquem l'úsuari
    {
      var preguntes_fetes = row[15];   // recuperem les preguntes queha contestat
      if(row[17] != "")    var punts0 = row[17];  // recuperem la puntuació tenint present la primera vegada que estarà buid
      else
      {
        sh.getRange(+i+1,18).setValue("0/0");
        var punts0 = "0/0" ;
      }

      var punts1 = punts0.split('/'); //Separem la puntuació del numero de preguntes per la barra
      var punts = punts1[0]; // En primer lloc a l'esquerra, element 0  tenim els encerts o punts
      var preg = punts1[1];  // A la dreta , elelment 1 , tenim el total de preguntes contestades  

      if(preg >=5 ) {  // Analitzem si ja hem acabat de fer el test
                     sendText(id_usuari,"Ja has acabat el questionari amb el resultat " + punts0 );
                     return true;
                     }
     break;      // Sortim del bucle perquè ja ha trobat l'usuari

    }

    }

// Carreguem les preguntes   
  var sh2 = SpreadsheetApp.openById(ssId).getSheetByName("Preguntes") ; // Accedim 1r a la fulla de càlcul i després al full que te per nom "Alumnes"
  var dades2 = sh2.getDataRange().getValues();  // Carreguen totes les file si columnes a una variable que anomenem dades
  var ultim = sh2.getLastRow() ;  // Cerquem el numero de preguntes per generar un numero al.leatòri


  var num_preg=0; // Inicialitzem el numero de pregunta

  while(num_preg == 0)  // Anem repetint fins que surti una pregunta que no ha fet
  {

     var posicio = Math.floor(Math.random() * ultim) +1  ;  // generem un al.leatòri que no pot ser 0, per això sumem 1

    if(String(preguntes_fetes).indexOf(posicio) == -1 ) // Si el numero que surt no està a la llista
   {
     num_preg = posicio ;   // Assignem la psoció queha sortit a num_preg per sortir del while
       if(posicio >=1)  sh.getRange(+i+1,16).setValue(preguntes_fetes + "-" + posicio); // Afegim el numero que ha sortit a la llista del full de càlcul
    }

  }

// Com ja tenim la posició de la pregunta carreguem les dades   
    var row = dades2[posicio];  //Carreguem en cada pas la fila posició de dades   
    var enunciat = row[0];   // Carreguem els camps que es comencen a comptar per l'index 0
    var res1  = row[1];
    var res2  = row[2];
    var res3  = row[3];
    var res_bona = row[4]; // Ara no la necessitem
   var frase = "<b>" + enunciat + "</b> \n \n " ; // Generem l'enunciat de la pregunta

   // Per visualitzar les possibles respostes ho farem amb botons que permetran clicar per escollir
   // Cada botó conte una crida a la funció de respostes amb l'opció i el nunero de pregunta

  var llista = new Array(
   [{"text": "a) .- " + res1 , "callback_data": "/res 1-" + posicio}],
   [{"text": "b) .- " + res2 , "callback_data": "/res 2-" + posicio}],
   [{"text": "c) .- " + res3 , "callback_data": "/res 3-" + posicio}] ) ;


  var tecles =  { inline_keyboard: llista    , resize_keyboard: true,one_time_keyboard : true  };

  sendText(id_usuari, "<b><i>" + enunciat + "</i></b>" ,tecles );   // Enviem l'enunciat i els 3 botons per clicar la resposta correcta

}
~~~
~~~
function resultat(id_usuari,text,idioma)
{
  var entra = text.split(' ');  // Recollim el text per analitzar l'opció i el numero d epregunta
  var res = entra[1].split('-'); // Separaem pel guió
  var resposta = res[0];  // A l'esquerra, índex 0 ens queda l'opció escollida
  var pregunta = res[1]; // A la dreta, index 1 el numero de pregunta


  var sheet = SpreadsheetApp.openById(ssId);
  var sh = sheet.getSheetByName("Alumnes") ; // Accedim 1r a la fulla de càlcul i després al full que te per nom "Alumnes"

  // refreshSheet(sheet,sh); // Si us apareix el missatge Nan que dona problemes pe rllegir del full afegiu aquesta funció que va accesibles les dades

  var dades = sh.getDataRange().getValues();  // Carreguen totes les files i columnes a una variable que anomenem dades

  for(i in dades)     // Localitzem l'usuari per recuperar les preguntes que ja ha fet
  {
    var row = dades[i];
    var num_usuari = row[14];

    if(num_usuari == id_usuari)
    {
      var resultats_fets = row[16]; // Recuperem els resultats fets i afegirem el d'aquesta pregunta

      if(row[17] != "")    var punts0 = row[17];  // Recuperem la puntuació per actualitzar-la
      else
      {
        sh.getRange(+i+1,18).setValue("0/0");
        var punts0 = "0/0" ;
      }



      var punts1 = punts0.split('/');  // Com hem fet abans amb la pregunta analitzem els encerts i el numero de preguntes fetes
      var preg = punts1[1];
      var punts = punts1[0];
      break;       

    }
   }


  var sh2 = SpreadsheetApp.openById(ssId).getSheetByName("Preguntes") ; // Accedim a la fulla i despres al full "Preguntes"
  var dades2 = sh2.getDataRange().getValues();                            
  var row = dades2[pregunta];  // Escollim la fila del numero de pregunta que hem rebut
  var correcte = row[4];  // Cerquem la columna5 índex 4 que te la posciió de la resposta correcte

  preg = parseInt(+preg+1) ;

  if(correcte == resposta )  // Cas que hi ha encert
  {

    punts = parseInt(+punts+1)  ;  // Sumem 1 als punts
    sh.getRange(+i+1,17).setValue(resultats_fets + "-1"); // Afegim un 1 que vol dir encert
    var res = " <b> Encert </b> [" + punts + "/" + preg + "]"  ;
  }
    else  // Si no hi ha encert
    {
    sh.getRange(+i+1,17).setValue(resultats_fets + "-0"); // Afegim un 0 que vol dir errada
    var res = " <b> Errada </b>  [" + punts + "/" + preg + "]"  ;
    }

  sh.getRange(+i+1,18).setValue(punts + "/" + preg);  // Contruim el resultat punts / respostes  


if( preg  < 5 )
    {                                   
        var llista = new Array(
             [{"text": "Nova pregunta"  , "callback_data": "/Questionari"}]) ; // Afegim el boró "Nova pregunta "

        var tecles =  { inline_keyboard: llista    , resize_keyboard: true,one_time_keyboard : true  };
       sendText(id_usuari, res  , tecles) ;
    }
    else
    {
         sendText(id_usuari,"Final del Questionari = " + punts + "/" + preg )  ;

    }


  return true;

}
~~~
~~~
function refreshSheet(spreadsheet, sheet) {
  var dataArrayRange = sheet.getRange(1,1,sheet.getLastRow(),sheet.getLastColumn());
  var dataArray = dataArrayRange.getValues(); // necessary to refresh custom functions
  var nanFound = true;
  while(nanFound) {
    for(var i = 0; i < dataArray.length; i++) {
      if(dataArray[i].indexOf('#N/A') >= 0) {
        nanFound = true;
        dataArray = dataArrayRange.getValues();
        break;
      } // end if
      else if(i == dataArray.length - 1) nanFound = false;
    } // end for
  } // end while
}
~~~

### 3.4 .- La nostra aula a Telegram

### La nostra aula a Telegram
- En aquesta activitat iniciarem el xatbot per gestionar el grup i canal de la nostra aula, que anirem ampliant en els temes posteriors . En aquesta activitat inicial desenvoluparem un mínim de 3 prestacions a la nostra aula, Queda a la vostra consideració, en funció del tipus de xatbot que vulgueu desenvolupar, que dediqueu esforços a fer crèixer aquest xatbot implementant prestacions que ja hem anat veient al llarg del curs, sigui en l'espai de les Jornades Docents o amb les altres activitats complementàries.

- Les implementacions que farem inicialment per la gestió són :

1. Implementació de perfils (alumne,delegat,professor)  i un menú keyboard que serà diferent per a cada perfil
2. Accés des de Telegram a la carpeta de materials del Drive. Aquesta carpeta haurà de permetre l'accés dels alumnes de l'aula.
3. Accés a les qualificacions personals obtingudes en el qüestionari 3.3

- Aquesta activitat és l'inici de l'espai de gestió docent d'una classe, En aquesta activitat inicial ens centrarem en aspectes de gestió, que no impedeix afegir aspectes d'informació i de docència, que podeu anar afegint .

Com a evidències a la presentació haureu d'enganxar captures de :

1. Dels perfils que has creat i de com es mostra de diferent la informació segons el perfil
2. Enganxa una captura que mostri a Telegram els fitxers existents a la teva carpeta compartida al Drive i també enganxar la url de la carpeta
3. Mostrar com es visualitza la informació privadament sobre les qualificacions al test


### Gestió de permisos i Menú
- Com hem comentat podem establir per exemple tres perfils d'usuari a l'aula: el d'alumne, el de delegat, i el de professor. Podríem establir-ne més en funció de nivells entre alumnes o entre professors.

a.- Començarem per afegir la columna perfils a la llista d'alumnat, sera una llista separada per comes
![](imgs/exercicis-950828fc.png)

b.- Crearem una funció per retornar els perfils de l'usuari a partir del seu id_usuari.

~~~
function perfil(id_usuari)
{
var sh = SpreadsheetApp.openById(ssId).getSheetByName("Alumnes");
  var dades = sh.getDataRange().getValues();  

  for(i in dades)
  {  

    var row = dades[i];
    var num_usuari = row[14];

    if(num_usuari == id_usuari)
    {
      return row[18]; // columna que conté els perfils

    }

  }

  return "";
}
~~~

![](imgs/exercicis-67c1e599.png)

c.- Quan vulguem aplicar un perfil recuperarem la llista de perfils, i amb la funció indexOf("perfil") > -1 detectarem si te o no el perfil indicat , anem-ho a aplicar al menú. Només si te a la llista el perfil "professor" afegirà el botó "Llistat" i el botó "Reunions" l'afegirà als perfils "professor" i "delegat" .

~~~
// funció per mostrar un Menú Keyboard en forma de botons sota de la barra de missatges
function menu(id_usuari,id)
{
 var permisos = perfil(id_usuari);  // Llegim els permisos de lúsuari
  var llista = new Array() ;  // Creem la llista de botons

  llista.push(["CodiQR","Qualificacions","Test"]);  // Afegim aquest 3 botons en una fila per a tothom

  if(permisos.indexOf("professor") > -1 )  llista.push(["Llistat"]); // Afegim el botó Llistat només pel professor
  if(permisos.indexOf("professor") > -1 || permisos.indexOf("delegat") > -1 )  llista.push(["Reunions"]);  // Afegim Reunions per al professor i delegat

                  var tecles =  { keyboard: llista    , resize_keyboard: true,one_time_keyboard : true  };  // Asigna la lista de botons a la comanda keyboard
               sendText(id,"Selecciona opció",tecles );  // Envia un missatge a Telegram emparnt la funció sendText()  
return true;  // retorna cert per a que no s'envii cap altre missatge
}
~~~

![](imgs/exercicis-67c1e599.png)

![](imgs/exercicis-cbab4651.png)

![](imgs/exercicis-8108d0e6.png)

- Molt important és tenir cura de filtrar desprès l'acces a les funcions restringides fent servir la mateixa instrucció. Si al cercar la paraula "professor" a la llista de permis, dona de resultat -1 vol dir que no hi é si per això fem return perquè no continuí .
~~~
   if(permis(id_usuari).indexOf("professor") == -1 )   return ;
~~~

Crea perfils a la teva aula i genera un Menú diferenciat per a cada perfil, en funció de les opcions que vulguis incorporar

### 2.- Carpeta del Drive amb accés des de Telegram
- Podem integrar el repositori de materials que normalment tenim al Drive dins de Telegram amb el que això representa de comoditat d'accés al núvol i a diferents dispositius .

Els passos a seguir són :

a.- Crear o localitzar la carpeta del Drive per a repositori de materials. Donar permís d'accés per enllaç i copiar el seu id
b.- Mirem d'activar els serveis Drive de Google de la nostra App
![](imgs/exercicis-8070a5f9.png)

c.- Podem assignar icones a cada tipus de fitxer, trobareu  la llista a https://apps.timwhitlock.info/emoji/tables/unicode
d.- Desenvoluparem el codi emprant les funcions especifiques de Google Apps per a l'accés als fitxers de Drive. Agruparem de 3 en 3  a la mateixa fila .

~~~
function drive(id,idioma)
{
 var emoji_audio = "🎧" ;
 var emoji_texto = "📋";
 var emoji_imagen = "🌆";
 var emoji_video = "🎥";
 var emoji_undefined = "💾";

 var folderId = "14OwfX92_QzThpYG42rTmNj03W8PASOaA";
  var carpeta = DriveApp.getFolderById(folderId);

  var llista = new Array();


  llista.push([{'text': "📂 " + carpeta.getName() , 'url': carpeta.getUrl() }]);

 var ficheros = carpeta.getFiles();

  var columna = 0;
  var fila = new Array();

  while (ficheros.hasNext()) {

    var fichero = ficheros.next();
    var tipo = fichero.getMimeType();
    var emoji = emoji_undefined;

    if(tipo.indexOf('image') > -1) var emoji = emoji_imagen;
    if(tipo.indexOf('text') > -1) var emoji = emoji_texto;
    if(tipo.indexOf('ogg') > -1) var emoji = emoji_audio;



    if(columna<3) {
              fila.push({'text': emoji + "  " + fichero.getName() , 'url': fichero.getUrl() });
              columna++;
    }
    else {
          llista.push(fila);
          fila = [];
          fila.push({'text': emoji + "  " + fichero.getName() , 'url': fichero.getUrl() });
         columna = 1 ;
        }

  }
           llista.push(fila); // afegim els fitxers que quedavan sense omplir la ultima fila

          var tecles =  { inline_keyboard: llista    , resize_keyboard: true,one_time_keyboard : true  };

          sendText(id,"Fitxers Drive",tecles );  
return true;
}
~~~
![](imgs/exercicis-bea8c32a.png)

### 3.- Consulta privada de les qualificacions
- Es evident que per realitzar aquesta tasca, cal tenir identificats els alumnes amb la seva id de Telegram, de manera que cercant pel seu id de Telegram tinguem accés a tota la informació de la seva fila. També emprant perfils podríem restringir informació.Per exemple si tinguéssim els pares al mateix grup, podríem afegir al perfil del fill el id dels pares de manera que desprès els pares o tutors podrien accedir a la informació del seu fill .

El procés a seguir seria :

- Utilitzaríem una comand aper accedir a les qualificacions per exemple Qualificacions o /notes
- Des del switch activariem la funció notes(id_usuari, idioma)
- En aquesta funció cercariem la id_usuari al full d'Alumnes i una vegada localitzat cercaríem les columnes que ens interessessin :
~~~
function notes(id_usuari,idioma)
{
var sh = SpreadsheetApp.openById(ssId).getSheetByName("Alumnes");
  var dades = sh.getDataRange().getValues();  

  for(i in dades)
  {  

    var row = dades[i];
    var num_usuari = row[14];

    if(num_usuari == id_usuari)
    {

      sendText(id_usuari," La teva qualificació al test és : " + row[17] );
      return row[18];

    }

  }

  return "";
}
~~~

![](imgs/exercicis-1d82c478.png)

---

## 3.5 .- Afegim a les Jornades Docents

### Afegim a les Jornades docents

- És molt important estructurar bé les aplicacions perque desprès sigui més fàcil modificar-les un cop fetes . Doncs ara anirem a treballar sobre l'aplicació creada en el projecte 2 que era el xatbot sobre la gestió d'unes Jornades Docents. I afegirem com a mínim dos implementacións més

- Recollir imatges de les jornades per fer un àlbum i fins i tot es podria fer un noticiari en format RSS
La validació de l'entrada en format QR


- Recordeu recuperar el codi del vostre xatbot de les Jornades, o el de l'activitat proposada

- Com en totes les activitats les evidencies per aquesta proposta les haureu d'enganxar a la presentació compartida. Deixant llibertat per presentar-ho de la manera que us agradi més o que creieu que queda millor .

### Recollida de imatges a una carpeta del Drive
- El que farem en aquest cas serà que quan s'envii una imatge mirarà el caption que porti i si porta la paraula "Imatge_Jornada" les guardarà en una determinada carpeta que haurem creat al Drive

El procés serà el següent :

  a.- Crearem o localitzarem una carpeta del Drive i copiarem el seu id .
  b.- Necessitem conèixer com passa paràmetres el Telegram quan enviem un document. Per això farem servir la funció del Mail i ens enviarem la informació per poder obtenir els paràmetres que utilitza : !!! Heu de personalitzar el vostre mail !!!
~~~
MailApp.sendEmail('email@@gmail.com', "JSON TELEGRAM" ,JSON.stringify(data,null,4) ) ;  
~~~

![](imgs/exercicis-c4541356.png)

c.- A continuació enviarem una imatge i veurem que rebem al mail .
![](imgs/exercicis-715a9bcd.png)

d.- I rebem el següent mail :
![](imgs/exercicis-e9326e9d.png)

~~~
{
     "update_id": 458676986,
     "message": {
         "message_id": 4071,
         "from": {
             "id": 1234567,
             "is_bot": false,
             "first_name": "Ferran",
             "username": "FerranBio",
             "language_code": "ca"
         },
         "chat": {
             "id": 1234567,
             "first_name": "Ferran",
             "username": "FerranBio",
             "type": "private"
         },
         "date": 1590302548,
         "photo": [
             {
                 "file_id": "AgACAgQAAxkBAAIP517KF1RY3WFeBQRaRPECxqq1YPh-AAKvsjEb6ONQUg0_MhvQJx_BFMhjI10AAwEAAwIAA20AA2UDAQABGQQ",
                 "file_unique_id": "AQADFMhjI10AA2UDAQAB",
                 "file_size": 26053,
                 "width": 239,
                 "height": 320
             },
             {
                 "file_id": "AgACAgQAAxkBAAIP517KF1RY3WFeBQRaRPECxqq1YPh-AAKvsjEb6ONQUg0_MhvQJx_BFMhjI10AAwEAAwIAA3gAA2MDAQABGQQ",
                 "file_unique_id": "AQADFMhjI10AA2MDAQAB",
                 "file_size": 26793,
                 "width": 258,
                 "height": 345
             }
         ],
         "caption": "Imatge Jornada Aqui podem veure el primer plat del menú
del catering de la Jornada"
     }
}
~~~

e.- A partir d'aqui podem veure com crear una nova entrada de tipus de contingut que serà la imatge, i que afegirem a la de "message" i "callback_query" que ja teníem. En el nostre cas gravarem en el full de càlcul la informació de la tramesa així com el text complementari que del contrari es perdria .
f.- Afegirem un full per guardar la informació de la tramesa, amb el nom de "Imatges"
g.- Enviem la imatge i en la Llegenda( Caption)  , Comencem amb l'etiqueta "Imatge_Jornades" :

![](imgs/exercicis-ee584ce2.png)

h.- El xatbot ens respondrà :

![](imgs/exercicis-c36f106b.png)

i .- Veurem que s'haurà gravat la imatge a la carpeta del Drive :
![](imgs/exercicis-31657f4d.png)

j .- I també s'haurà gravat el registre al full de càlcul :

![](imgs/exercicis-ea24ea73.png)

El codi per aplicar aquesta implementació és el següent : ( cal afegir en el doPost ) :

~~~
//  afegir desprès del data.message i del data.callback_query

  if(data.message.photo)  // En cas de que enviem una imatge
{
var text = data.message.caption;  // Recupera el text de la imatge
var id = data.message.chat.id;  // Recupera el id de la finestra d'on procedeix el missatge
var id_usuari = data.message.from.id; // Recupera el id de l'usuari que ha escrit el missatge
var id_missatge = data.message.message_id; // Recupera el id del missatge
var update_id =   data.update_id; // Recupera el id del missatge final
var lang = data.message.from.language_code ;  // Recupera l'idioma que te el Telegram de l'usuari que ha enviat el missatge
var nom = data.message.from.first_name ;  // Recupera tot el nom de l'usuari que ha enviat el missatge
var location = data.message.location;



 if(text.indexOf("Imatge_Jornades") > -1 ) { // filtrar que només tracti el que tinguin aquesta etiqueta en la Llegenda

var folderId = "1fRyzpAXSDLQVMRwULdyR22TqDj5WSr7V";  // Id de la carpeta del Drive que recollira les imatges

var file_id= data.message.photo[0].file_id;   /7 Important que indiquem la fila 0 perque es un array una llista
var url_api_document = telegramUrl + "/getFile?file_id=" + file_id ;  // Demanem a Telegram més informació sobre aquesta imatge, per conèixer la seva url per fer-ne la descarrega



  var response_document = UrlFetchApp.fetch(url_api_document);  // Accedeix a la informació de Telegram sobre la imatge
  var json_document= response_document.getContentText(); // Agafa informació rebuda coma resposta de text
  var dades2 = JSON.parse(json_document); //Parseja el text rebut

  Logger.log(dades2); //Fem un log per detectar problemes

  var path_document = dades2.result.file_path;  // Construim la url del document final



 var documentUrl = "https://api.telegram.org/file/bot" + token + "/" + path_document ; // Accedim a aquest fitxer nviat

     var response = UrlFetchApp.fetch(documentUrl, {followRedirects: true }); // Recuperem el contingut del fitxer

         folder = DriveApp.getFolderById(folderId);  // Ens situem en la carpeta de fotos de les Jornades
          var blob = response.getBlob();   // Carreguem la imatge
          var file = folder.createFile(blob);  // Creem un fitxer amb les dades de la imatge

          // file.setName(file_name); //( Posem nom al fitxer
         var file_name = file.getName() // Mirem quin nom li ha posat

          var url_fitxer = "https://drive.google.com/uc?export=view&id=" + file.getId();
          var trobat = true;

  SpreadsheetApp.openById(ssId).getSheetByName("Imatges").appendRow([new Date(),id_usuari,text,file_name,url_fitxer]);  // Afegim al full de calcul

   var enviat = true ; // Perque no envii més missatges

  sendText(id, "S'ha guardat a la carpeta del Drive,la imatge enviada. \n " + url_fitxer);  

 } // fi del filtre "Imatge Jornades"   

} // fi del data.message.photo

~~~

- Intenteu implementar aquesta opció al Xatbot de les Jornades per fer la recollida d'imatges de les mateixes de manera senzilla des del mateix Telegram a la vegada que queden en el Telegram, les tindrem al Drive i en el full de càlcul per fer-ne el tractament que considerem , com per exemple crear un RSS per poder-les visualitzar a la web del centre, o un inline_bot per agilitzar el seu reenviament

### Validació de l'entrada QR

- En l'anterior Unitat ja varem generar un codi QR que s'enviava per email a cada participant inscrit en forma de codi QR . Ara incorporarem la lectura d'aquest codi , que s'entén es farà a l'entrada al recinte o a cada taller o activitat de les Jornades  .

El procés que seguiríem seria :

a.- Afegir l'opció de generar el codi QR des del mateix xatbot per en cas de que no tingues el qr del correu electrònic disponible . Aplicaríem la següent funció :

~~~
function qr(id_usuari,idioma)
{
        var  caption = "Entrada a les III Jornades";

        var url_script = webAppUrl +"?accio=validar%26id=" + id_usuari ;
        var qr = "https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=" + url_script;


       var response = UrlFetchApp.fetch(qr);
       var binaryData = response.getContent();


         var blob = Utilities.newBlob(binaryData, 'image/jpeg', 'QR');
         sendPhoto3(id_usuari,blob, caption);

        return true;                      


}
~~~
![](imgs/exercicis-6dba5a10.png)

b,. Haurem de tenir creada la funció doGet per llegir entrades amb metode get , en el Tema 2 ja tenieu una primera versió .

![](imgs/exercicis-267c5209.png)

Ara activarem la validació creant una funció validar(id) que cridarem des de l'entrada doGet :

~~~
function doGet(e)
{
 var accio = e.parameter.accio;
 var id = e.parameter.id;

   switch (accio)
    {
      case 'validar' :
          var res = validar(id);
          return ContentService.createTextOutput(res) ;  
      break;    
     }
}


function validar(id_usuari)
{
var sh = SpreadsheetApp.openById(ssId).getSheetByName("Participants");  // Accedim al full de càlcul
var dades = sh.getDataRange().getValues();  // Llegim dels dades de la fila 2, columna 1, fins la ultima fila, columna 14

   for(i in dades)  // Cerquem l'usuari a partir del seu id
  {
    var row = dades[i];
    var num_usuari = row[11];

    if(num_usuari == id_usuari)
    {
   var entrada =   row[15];  
   var data_entrada = row[16];
   var nom = row[1];
   var cognoms = row[2];

  if(entrada.length  <3)
  {
    sh.getRange(+i+1,16).setValue("Entrat");  
    sh.getRange(+i+1,17).setValue(new Date());  
    return " Entrada correcte pel " + nom + " " + cognoms ;
    }  
      else return " Entrada utilitzada pel " + nom + " " + cognoms ;  
 }
 }
   return " Problema amb l'entrada "  ;
 }

~~~

c.- Una vegada llegim el codiQr amb un lector des del dispositiu mòbil qualsevol , succeirà el següent :
Lectura del codi QR :

![](imgs/exercicis-97013e66.png)

![](imgs/exercicis-3b01efbd.png)

- El que fa el lector de codiQR es descodificar la url que tindrà aquesta sintaxi :

~~~
https://url_del_nostre_script?accio=validar&id=id_Telegram_del_usuari
~~~

  - Quan cliquem el lector enviara la url a un navegador que accedirà al nostre script per la porta del mètode GET i li transmetrà dos paràmetres: acció i id , que seran recollits i utilitzats per la validació

d.- Abans de llegir les dades en el full de càlcul dels participants veuríem :

![](imgs/exercicis-b0179b1d.png)

  - Una vegada validada l'entrada omple les següents dades :

![](imgs/exercicis-4f8b0cf2.png)

e.- Si intentes tornar a validar , detectaria que ja el text "Entrat" o per la data i ens informaria :

![](imgs/exercicis-24aed609.png)

- Per garantir la seguretat i evitar lecturas del codi al marge, es pot aplicar un lector propi que afegeixi un codi a la url de manera que en la validació caldria tenir present aquest codi que no estaria en el codiQr

---
### Entrega del Projecte 3

- En aquesta unitat haureu d'anar enganxat les evidencies en una plantilla de presentació de Google Slides, que tindreu ja pautada per simplificar la tasca, cliqueu aquí per accedir a la plantilla: https://drive.google.com/open?id=1raewRQRj9mb9D-CvWQkCc2-jc4VWqDeVqG8sjHzvXIQ

- Repassa les evidencies de les diferents activitats que contempla aquest Tema 3 :

1. Activitat 3.1 Dues captures d'aplicació entre les 10 FAQS
2. Activitat 3.2  La incorporació al Xatbot d'un webservice
3. Activitat 3.3 La incorporació d'un registre log de tots els missatges que arriben al grup i La incorporació d'un qüestionari de preguntes auto-avaluables pels alumnes
4. Activitat 3.4 La definició de perfils , l'accés a una carpeta del Drive i la consulta privada de notes
5. Activitat 3.5 La implementació a les Jornades de la recollida d'imatges i la validació de l'entrada QR

![](imgs/exercicis-b1de5944.png)
