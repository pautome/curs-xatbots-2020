# Projecte 2: Implementació de la gestió d'una Jornada docent amb Telegram

- Referències exemples de codi googleapps script - http://googleappscripting.com/doget-dopost-tutorial-examples/

- Referència Apps Script Services - https://developers.google.com/apps-script/reference/
- Referència servei MailApp - https://developers.google.com/apps-script/reference/mail/mail-app

- Creació d'un bot Telegram amb scripts de google - https://medium.com/@unnikked/how-to-create-your-first-telegram-bot-9005c08a5aa5

- How to easily build a Telegram Bot with Hook.io - https://unnikked.ga/how-to-easily-build-a-telegram-bot-with-hook-io-4c144c4abd96 - http://hook.io/

- video tutorial per crear bots amb google app script - https://www.youtube.com/playlist?list=PLGGHwNnXfci86dfqIVLc5l391SPk-RX1F

- Visor JSON per Chrome - https://chrome.google.com/webstore/detail/jsonview/chklaanhfefbnpoihckbnefhakgolnmc

## Creació de formularis Google per a recollida de dades

- Crearem el formulari amb les entrades indicades. Es pot aplicar diferents filtres per a validar les entrades de l'usuari (per exemple, amb expressions regulars).

- Cada entrada genera una columna en el full de càlcul associat.

![](imgs/02-xatbots-2020-d6f4ca99.png)

- Cada resposta al formulari crearà una fila al full de càlcul. Per associar el full de càlcul, anem a "Respostes" i piquem la icona de full de càlcul. Si no n'hi ha un d'associat, cal indicar quin serà.

![](imgs/02-xatbots-2020-4f45484a.png)

![](imgs/02-xatbots-2020-186d623f.png)

- Es pot associar un script al full de càlcul.

![](imgs/02-xatbots-2020-59780aee.png)

- Cada funció que es defineixi a l'editor de codi es pot associar a un event.

~~~
//Id del full de calcul
// https://docs.google.com/spreadsheets/d/1pRfPL34PL3N0nvpTyW5mPQybM1VhMOGFfa7m-u0cMmI
var ssId = "1pRfPL34PL3N0nvpTyW5mPQybM1VhMOGFfa7m-u0cMmI" ;

// URL de l'escript
// var url_script = "https://script.google.com/macros/s/AKfycbxikrZfE6eMqy2G68wqZw2nT2ti61xjyqoJx4Z4u7wROGUg3Bc/exec" ;
var url_script = "https://script.google.com/macros/s/AKfycbx-Mi2Pf2MErz3WO7WSgFby52bTRWxpVBT5INd7sgArhNtu7Rc6/exec" ;

// Funció de proves
function doGet(e)
{
 var accio = e.parameter.accio;
 var id = e.parameter.id;

   switch (accio)
    {
      case 'validar' :
          return ContentService.createTextOutput("He rebut acció validar del id = " + id ) ;  
      break;    
     }
}

// Funció per a enviar missatge de confirmació a l'últim element del full de càlcul
function envia_mis()
{

var sh = SpreadsheetApp.openById(ssId) ; // identifica el full de càlcul
var sheet = sh.getSheets(); // Agafa totas els fulls
var dades = sheet[0].getDataRange().getValues(); // Agafa nomes el subfull segon de index 1

var ultim = sheet[0].getDataRange().getLastRow(); // Localitzem la posició de la última fila
var fila = dades[ultim-1]; // Carreguem la ultiam fila en una fila de treball

var timestamp = fila[0]; // Agafem la priemra columna d'index 0 que porta l'hora de gravació
var nom = fila[1]; // Agafem el nom de l'usuari a la columna 2 index 1
var cognoms = fila[2]; // Agafem els cognoms de l'usuari a la columna 3 index 2
var nif = fila[3]; // Agafem el nif de l'usuari a la columna 4 index 3
var mail = fila[5]; // Agafem el email del usuari a la columan 5 index 4
var franja1 = fila[9]; // Agafem el taller de la franja1  a la columna 9 index 8
var franja2 = fila[10]; // Agafem el taller de la franja2  a la columna 10 index 9


var url_acces= url_script+"?accio=validar%26id=" + ultim ;
var qr = "https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl="+ url_acces ;
 var img = UrlFetchApp
                         .fetch(qr)
                         .getBlob()
                       .setName("img");


var subject = "Inscripció a les Jornades " ;

var plain_email_body = "Gracies per la teva inscripció  " + nom +" !! ! \n\n " +
                       "Has formalitzat la teva inscripció a les III Jornades \n" +
                       "Els tallers que t'has inscrits són :  \n " +
                       "   Franja 1 : " + franja1 + "\n" +
                       "   Franja 2 : " + franja2 + "\n\n\n" +
                       "Aquest codiQR que t'adjuntem l'hauras de mostrar a l'entradada de les jornadas i als Tellers per validar la teva presencia";

var html_body =       "Gracies per la teva inscripció  " + nom +" <br /><br />" +
                       "Has formalitzat la teva inscripció a les III Jornades <br />" +
                       "Els tallers que t'has inscrits són :  <br />" +
                       "   Franja 1 : " + franja1 + "<br/>" +
                       "   Franja 2 : " + franja2 + "<br /><br /><br />" +
                       "Aquest codiQR que t'adjuntem l'hauras de mostrar a l'entradada de les jornadas i als Tellers per validar la teva presencia <br />";

var advancedOpts = { name: "Inscripció III Jornades", htmlBody: html_body ,body: img,attachments: img};

MailApp.sendEmail(mail, subject, plain_email_body, advancedOpts );

}
~~~

- Per associar les funcions a l'event cal un disparador (trigger). Picar el botó amb la icona de rellotge.

![](imgs/02-xatbots-2020-41194158.png)

- Fem "afegir disparador" o editar un que ja existeixi (icona de llapis):

![](imgs/02-xatbots-2020-d6971f89.png)    

- Podem escollir la funció que s'executa (envia_mis), on es genera l'event, quan s'executa (en enviar-se el formulari) i com ens notifica errors.

- Ara cal "publicar" el codi. A l'editor de codi, piquem "Publicar".

![](imgs/02-xatbots-2020-66e2ca33.png)

- Omplim els camps "Project version", que serà "nou" cada vegada que fem canvis al codi; Executar script amb l'usuari que volem (el nostre per evitar canvis en permisos) i qui pot accedir a l'App.

- Quan omplim el formulari i li donem a enviar s'activa l'script que ens envia un correu electrònic de confirmació

![](imgs/02-xatbots-2020-6751bef5.png)

---

## Crreació dels certificats

- Necessitem 3 documents:

  - Full de calcul amb els inscrits, que genera el formulari. https://docs.google.com/spreadsheets/d/1pRfPL34PL3N0nvpTyW5mPQybM1VhMOGFfa7m-u0cMmI/edit#gid=1504834781
  - Plantilla del certificat, que tindrà els camps per posar nom, cognoms i nif als espais adients. https://docs.google.com/document/d/1KRvJ_z7-VR4pYn4GGdAOoQg7_I43Wns7CAWb5Z2iTIw/edit
  - Document final que contindrà tots els certificats un darrera l'altre que es creen a partir de la plantilla i les dades del full de participants (combinació). https://docs.google.com/document/d/1NQoIz_xjzK5IByDIbNi7W-94SIYnIBjlPWVYSlkhE1I/edit


- Afegirem el codi i quedarà així:

~~~

//Id del full de calcul
// https://docs.google.com/spreadsheets/d/1pRfPL34PL3N0nvpTyW5mPQybM1VhMOGFfa7m-u0cMmI
var ssId = "1pRfPL34PL3N0nvpTyW5mPQybM1VhMOGFfa7m-u0cMmI" ;

// URL de l'escript
// var url_script = "https://script.google.com/macros/s/AKfycbxikrZfE6eMqy2G68wqZw2nT2ti61xjyqoJx4Z4u7wROGUg3Bc/exec" ;
var url_script = "https://script.google.com/macros/s/AKfycbx-Mi2Pf2MErz3WO7WSgFby52bTRWxpVBT5INd7sgArhNtu7Rc6/exec" ;

plantilla_Id = "1KRvJ_z7-VR4pYn4GGdAOoQg7_I43Wns7CAWb5Z2iTIw";      // Id de la plantilla del certificat     
final_Id = "1NQoIz_xjzK5IByDIbNi7W-94SIYnIBjlPWVYSlkhE1I";           // Id de la copia de la plantilla

// Funció de prova
function doGet(e)
{
 var accio = e.parameter.accio;
 var id = e.parameter.id;

   switch (accio)
    {
      case 'validar' :
          return ContentService.createTextOutput("He rebut acció validar del id = " + id ) ;  
      break;    
     }
}

// Funció per a enviar missatge de confirmació a l'últim element del full de càlcul
function envia_mis()
{

var sh = SpreadsheetApp.openById(ssId) ; // identifica el full de càlcul
varMockaroo.com sheet = sh.getSheets(); // Agafa totas els fulls
var dades = sheet[0].getDataRange().getValues(); // Agafa nomes el subfull segon de index 1

var ultim = sheet[0].getDataRange().getLastRow(); // Localitzem la posició de la última fila
var fila = dades[ultim-1]; // Carreguem la ultiam fila en una fila de treball

var timestamp = fila[0]; // Agafem la priemra columna d'index 0 que porta l'hora de gravació
var nom = fila[1]; // Agafem el nom de l'usuari a la columna 2 index 1
var cognoms = fila[2]; // Agafem els cognoms de l'usuari a la columna 3 index 2
var nif = fila[3]; // Agafem el nif de l'usuari a la columna 4 index 3
var mail = fila[5]; // Agafem el email del usuari a la columan 5 index 4
var franja1 = fila[9]; // Agafem el taller de la franja1  a la columna 9 index 8
var franja2 = fila[10]; // Agafem el taller de la franja2  a la columna 10 index 9


var url_acces= url_script+"?accio=validar%26id=" + ultim ;
var qr = "https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl="+ url_acces ;
 var img = UrlFetchApp
                         .fetch(qr)
                         .getBlob()
                       .setName("img");


var subject = "Inscripció a les Jornades " ;

var plain_email_body = "Gracies per la teva inscripció  " + nom +" !! ! \n\n " +
                       "Has formalitzat la teva inscripció a les III Jornades \n" +
                       "Els tallers que t'has inscrits són :  \n " +
                       "   Franja 1 : " + franja1 + "\n" +
                       "   Franja 2 : " + franja2 + "\n\n\n" +
                       "Aquest codiQR que t'adjuntem l'hauras de mostrar a l'entradada de les jornadas i als Tellers per validar la teva presencia";

var html_body =       "Gracies per la teva inscripció  " + nom +" <br /><br />" +
                       "Has formalitzat la teva inscripció a les III Jornades <br />" +
                       "Els tallers que t'has inscrits són :  <br />" +
                       "   Franja 1 : " + franja1 + "<br/>" +
                       "   Franja 2 : " + franja2 + "<br /><br /><br />" +
                       "Aquest codiQR que t'adjuntem l'hauras de mostrar a l'entradada de les jornadas i als Tellers per validar la teva presencia <br />";

var advancedOpts = { name: "Inscripció III Jornades", htmlBody: html_body ,body: img,attachments: img};

MailApp.sendEmail(mail, subject, plain_email_body, advancedOpts );

}

// Generació dels diplomes de forma automàtica

 function diploma()
{

  // full_Id = "1zhoiq_Y8ZBXCLjQHtqB8aHzNXbSJVCDX3wSuYmM";       // Id del full de càlcul
  full_Id = ssId;                                                // Id del full de càlcul
  //plantilla_Id = "1Q_wLvH0pm9CJHXAzObtLflSOj9J4cucoISDnr8";      // Id de la plantilla del certificat     
  //final_Id = "1OEjGJHcocqwU72029MYHifeVxbTBkT7ZLwys";           // Id de la copia de la plantilla

  var plantilla = DocumentApp.openById(plantilla_Id);   // Obrim la plantilla
  var final = DocumentApp.openById(final_Id);       // Obrim la copia de la plantilla
  var plantilla_paragrafs = plantilla.getBody().getParagraphs();   // Carreguem tots els paràgrafs de la Plantilla

  // Al full de càlcul, obrim el full (pestanya anomenat "Participants", per tant cal canviar el nom a la pestanya)
  var sh = SpreadsheetApp.openById(full_Id).getSheetByName("Participants");  // Accedim al full de càlcul
  var dades = sh.getRange(2,1,sh.getLastRow()-1,14).getValues();   // Llegim dels dades de la fila 2, columna 1, fins la ultima fila, columna 14


  final.getBody().clear();  // Esborrem la plantilla Final per començar de nou un certificat


// r[1], r[2] i  r[4] correspon a les posicions de columna de les dades que seran enviades i que son nom, cognoms i nif


  dades.forEach(function(r){  // funció que es va repetint per cada fila de dades
        combinat(r[1],r[2],r[4],plantilla_paragrafs,final );    // Envia les dades a la funció combinat per fer la subtitució
  })   // fi de la funcio forEach

}   // fi de la funció diploma



function combinat(nom,cognoms,nif,plantilla_paragrafs,final)    //funció que fa la substitució
{
  plantilla_paragrafs.forEach(function(p){    // per cada paragraf

    final.getBody().appendParagraph(p               // Es situa en el document copia de plantilla
                                    .copy()
                                    .replaceText("{nom}",nom)   // Reemplaça el nom
                                    .replaceText("{cognoms}",cognoms) //Reemplaça els cognoms
                                    .replaceText("{nif}",nif) //Reemplaça nif
                                    );
  })   // fi de la funció forEach

   final.getBody().appendPageBreak();  // Si volem que els certificats estiguin junts en un unic document anem afegint pagines

}
~~~
- Publiquem com abans l'App web.
- Podem crear dades de prova amb https://www.mockaroo.com/

![](imgs/02-xatbots-2020-7d69ed66.png)

- Ara descarreguem les dades generades (format csv per exemple) i les enganxem al full de càlcul per simular les inscripcions de 50 persones - [Full amb dades][87a5f0cb]

- Executem a l'editor de codi la funció "diploma" i ens generarà un document de google amb tots els certificats un darrera l'altre

![](imgs/02-xatbots-2020-a7a5adaf.png)

- [Document amb certificats composats][87a5f0ca]

[87a5f0cb]: https://docs.google.com/spreadsheets/d/1pRfPL34PL3N0nvpTyW5mPQybM1VhMOGFfa7m-u0cMmI/edit#gid=1504834781 "Full dades"
[87a5f0ca]: https://docs.google.com/document/d/1NQoIz_xjzK5IByDIbNi7W-94SIYnIBjlPWVYSlkhE1I/edit
 "Doc certificats"

---

## Creació del xatbot

- Ara afegim el codi per a crear script Google apps script per a comunicar-nos amb el bot que havíem creat.

~~~
/*--------------------------------------------------Formes de depurar
 1. Crear funcions que simulin la crida des de fora

function testCertificat(){
  certificat("379302885","ca");
}

function testIden(){
  iden("379302885","","ca");
}

function testEscriure_frase(){
  escriu_frase("379302885","Prova");
}

- Depurar amb debugger: Picant a l'esquerra de l'editor de codi podem posar breakpoint
- Picant el "bitxo" activem depurador.

2. Enviar missatges de log quan passa alguna cosa. Despŕes veure amb "mostra->registres".

Logger.log('Emailing data row ' + rowNumber + ' to ' + email);

-----------------------------*/

// id per enviar debug
var id_debug=0;

// -----------------------------Enviar missatge de debug al bot
function debug(text){
  sendText(id_debug,"DEBUG: "+ text);
}

//  BOT-PTOME2 @PTOME2_BOT  http:/t.me/jornades_bot

var token = "1267376424:AAFznuTus_WFwPAorrLBB54fkc317N-hBNc";  // Api token del vostre bot que obtindreu al BotFather
var telegramUrl = "https://api.telegram.org/bot" + token;
// Canviat nou codi
var webAppUrl = "https://script.google.com/macros/s/AKfycbzgZDBRwgLazceacT_zZsrn12C8H2BBYQyGSyvROIqLFj52JkM/exec";  // Url de l'escript de Google

// plantilla Pau
var plantilla_Id = "1KRvJ_z7-VR4pYn4GGdAOoQg7_I43Wns7CAWb5Z2iTIw";      // Id de la plantilla del certificat     
var final_Id = "1NQoIz_xjzK5IByDIbNi7W-94SIYnIBjlPWVYSlkhE1I";           // Id de la copia de la plantilla

// Canviat per full nou
var ssId = "15Y74Zs8WIJ4WyL8U18SNWLhKBI1lM7-6Oo1fZq83zsQ";  // Id del full de càlcul que esteu treballant
var ID_canal = "";
var id_grup = "";
var folderId = "";

// https://api.telegram.org/botAPI_KEY/getUpadtes
//https://api.telegram.org/botAPI_KEY/setWebhook?url=url_WebAppUrl


/* Opcions de comandes del bot
ajuda - Informació sobre el bot
info - Informació sobr eles Jornades
tallers - Tallers "379302885","ca"ts
entrada - Codi QR per entrar o validar-se
certificat - Certificat d'assistencia
idioma - Selecciona d'idioma de comunicació del xatot
iden - Identificació
*/


// Funcions per enviar a Telegram

function sendPhoto(id,foto,caption)
{
var url = telegramUrl + "/sendPhoto?chat_id=" + id + "&photo=" + foto+"&caption=" + caption ;

  var response = UrlFetchApp.fetch(url);
  Logger.log(response.getContentText());
}

function sendDocument(id,fitxer)
{
var url = telegramUrl + "/sendDocument?chat_id=" + id + "&document=" + fitxer;
var response = UrlFetchApp.fetch(url);
Logger.log(response.getContentText());
}



function sendDocument2(chatId,id,caption){

  var fileId = id ;
  var img = DriveApp.getFileById(id);  
  var blob2 = img.getBlob().getAs("text/plain");


  var payload = {
          method: "sendDocument",
          chat_id: String(chatId),
          photo: blob2,
          caption : caption,
          parse_mode: "HTML"
          //disable_web_page_preview: true,
  };

  var options = {
    method: "POST",
    payload: payload,
    muteHttpExceptions : true
  };

   var request = UrlFetchApp.fetch( telegramUrl + '/', options);
   Logger.log(request.getContentText());
 }



function sendVideo(id,fitxer)
{
var url = telegramUrl + "/sendVideo?chat_id=" + id + "&video=" + fitxer;
var response = UrlFetchApp.fetch(url);
Logger.log(response.getContentText());
}


function deleteMessage(id,id_missatge)
{
var url = telegramUrl + "/deleteMessage?chat_id=" + id + "&message_id=" + id_missatge;
var response = UrlFetchApp.fetch(url);
Logger.log(response.getContentText());
}



function sendLocation(id,lat,long)
{
var url = telegramUrl + "/sendlocation?chat_id=" + id + "&latitude=" + lat + "&longitude=" + long ;
var response = UrlFetchApp.fetch(url);
Logger.log(response.getContentText());
}


function getFile(file_id) {
  var url = telegramUrl + "/getFile?file_id=A...EC";
  var response = UrlFetchApp.fetch(url);
   Logger.log(response.getContentText());
}



/* Codis HTML permessos per Telegram

<b> negreta </b>, <strong> negreta </strong>
<i> cursiva </i>, <em> cursiva </em>
<u> subratllar </u>, <ins> subratllar </ins>
<s> strikethrough </s>, <strike> strikethrough </strike>, <del> strikethrough </del>
<b> negreta <i> cursiva negreta <s> cursiva en negreta en cursiva </s> <u> subratlla en negreta en cursiva </u> </i> negreta </b>
<a href="http://www.example.com/"> URL en línia </a>
<a href="tg://user?id=123456789"> esment en línia d'un usuari </a>
<code> codi d'amplada fixa en línia </code>
<pre> Bloc de codi de l'amplada fixa preformatat </pre>
<pre> <code class = "language-python"> bloc de codi preformatat d’amplada fixa escrit en el llenguatge de programació de Python </code> </pre>

*/

//-----------------------------------------------Enviar text a un chat de telegram
function sendText(chatId,text,keyBoard){
  keyBoard = keyBoard || 0;


  if(keyBoard.inline_keyboard || keyBoard.keyboard){
     var data = {
      method: "post",
      payload: {
         method: "sendMessage",
         chat_id: String(chatId),
         text: text,
         parse_mode: "HTML",
         reply_markup: JSON.stringify(keyBoard)
       }
     }
    }else{
      var data = {
        method: "post",
        payload: {
          method: "sendMessage",
          chat_id: String(chatId),
          text: text,
          parse_mode: "HTML"
        }
      }
    }

   UrlFetchApp.fetch( telegramUrl + '/', data);

 }



function sendText3(id,text)
{
//r url = telegramUrl + "/sendMessage?chat_id=" + id + "&text=" +text +"&parse_mode=html" ;
 url = telegramUrl + "/sendMessage?chat_id=" + id + "&text=" +text ;
var response = UrlFetchApp.fetch(url);
Logger.log(response.getContentText());
}


function sendPhoto2(chatId,id,caption){

  var fileId = id ;
  var img = DriveApp.getFileById(id);  
  var blob2 = img.getBlob();


  var payload = {
          method: "sendPhoto",
          chat_id: String(chatId),
          photo: blob2,
          caption : caption,
          parse_mode: "HTML"
          //disable_web_page_preview: true,
  };

  var options = {
    method: "POST",
    payload: payload,
    muteHttpExceptions : true
  };

   var request = UrlFetchApp.fetch( telegramUrl + '/', options);
   Logger.log(request.getContentText());
 }


function sendPhoto3(chatId,blob2,caption){

  var payload = {
          method: "sendPhoto",
          chat_id: String(chatId),
          photo: blob2,
          caption : caption,
          parse_mode: "HTML"

          //disable_web_page_preview: true,
  };

  var options = {
    method: "POST",
    payload: payload,
    muteHttpExceptions : true
  };

   var request = UrlFetchApp.fetch( telegramUrl + '/', options);
   Logger.log(request.getContentText());
 }


function downloadFile(fileURL,folder) {

  var fileName = "";
  var fileSize = 0;

  var response = UrlFetchApp.fetch(fileURL, {muteHttpExceptions: true});
  var rc = response.getResponseCode();

  if (rc == 200) {
    var fileBlob = response.getBlob()
    var folder = DocsList.getFolder(folder);
    if (folder != null) {
      var file = folder.createFile(fileBlob);
      fileName = file.getName();
      fileSize = file.getSize();
    }
  }

  sendText(id,"Gravat");
}


// Fi de funcions de Telegram

// ----------------------------------Quan es rebi les dades via post d'un telegram
function doPost(e) {

  var data = JSON.parse(e.postData.contents); // Assigna les dades pasades per Telegram en format JSON a una variable data


if(data.message)  // En cas de que no fem servir callback
{

var text = data.message.text;  // Recupera el text del missatge
var id = data.message.chat.id;  // Recupera el id de la finestra d'on procedeix el missatge
var id_usuari = data.message.from.id; // Recupera el id de l'usuari que ha escrit el missatge
var id_missatge = data.message.message_id; // Recupera el id del missatge
var update_id =   data.update_id; // Recupera el id del missatge final
var lang = data.message.from.language_code ;  // Recupera l'idioma que te el Telegram de l'usuari que ha enviat el missatge
var nom = data.message.from.first_name ;  // Recupera tot el nom de l'usuari que ha enviat el missatge
var location = data.message.location;

}

if(data.callback_query)
  {
  var id_usuari = data.callback_query.from.id;
  var id = data.callback_query.message.chat.id;   
  var id_missatge = data.callback_query.message.message_id; // Recupera el id del missatge
  var text = data.callback_query.data;
  var usuari =  data.callback_query.from.user_name;
  var nom =  data.callback_query.from.first_name;
  var lang =  data.callback_query.from.language_code;
 }  


id_debug=id_usuari;

  var idioma= assigna_idioma(id_usuari,lang);

  var enviat = false;

var entrada = text.split('@');  // Separa les paraules entrades en una matriu/array per tenir per una banda la comanda i d'altre els valors
var comanda = entrada[0]; //La comanda serà la primera paraula. Es comença a comptar per zero

var comanda0 = comanda.split(' ');
var comanda = comanda0[0];   


  debug("Comanda Rebuda: "+ comanda + " "+text);  


switch(comanda)
      {
        case '/start'  :   // Fabrica el missatge de benvinguda en l'idioma del Telegram de l'usuari
             var resposta = escriu_frase('Benvingut/Benvinguda al xatbot',idioma);
        break;

        case '/ajuda':   
          var enviat = ajuda(id_usuari,idioma);
        break;

        case '/info':   
            var enviat = info(id_usuari,idioma);
        break;  

        case '/iden':   
            var enviat = iden(id_usuari,text,idioma);
        break;  

        case '/entrada':   
            var enviat = qr(id_usuari,idioma);
        break;  

        case '/idioma' :

          var enviat = menu_idioma(id_usuari,idioma);
          break;

        case  '/idi' :
           var enviat = canvia_idioma(id_usuari,text) ;
          break;


        default   :
          var resposta = "No entenc què vol dir: " + text  ;
             break;
    }

  if(enviat != true) sendText(id,resposta);  // Envia la resposta
}


//-----------------------Quan l'app web de Google rep petició via GET (?parametre=valor&parametre2=valor2)

function doGet(e)
{
 var accio = e.parameter.accio;
 var id = e.parameter.id;

   switch (accio)
    {
      case 'validar' :
          return ContentService.createTextOutput("He rebut acció validar del id = " + id ) ;  
      break;    
     }
}

// ------------------------------------------Agafar text i traduir a idioma abans de mostrar
function escriu_frase(frase,idioma)
{

  debug("Entrem a Escriu_frase");


  if(idioma.length == 2  && idioma !="ca") {
    debug("Llengua no CA");
    var frase= LanguageApp.translate(frase, 'ca', idioma); // Si els dos idiomes coincideixen dona error, per això excluim fer traducció si l'idioma de l'usuari és el catalè
  }

  return frase ;
}


function ajuda(id,idioma)
{


var frase = " Aquest Xatbot te com objectiu ajudar-te en el desenvolupament de les <b>III Jornades Docents </b> " +
             "Pots accedir a les comandes del xatbot clicant sobre la '/' i escollint l'opció desitjada \n\n "+
             "Les comandes que pots activar directament son:  \n\n" +  
             "/ajuda - Informació sobre el bot \n " +
             "/info -  Informació sobre les Jornades \n"+
             "/tallers - Tallers assignats  \n "+
             "/entrada - Codi QR per entrar o validar-se \n"+
             "/certificat - Certificat d'assistencia  \n " +
             "/idioma - Selecciona l'idioma de comunicació del xatbot \n " +
             "/iden - Identificació \n ";

  sendText(id,escriu_frase(frase,idioma));   

  return true;


}


function info(id_usuari, idioma )
{

   var sh = SpreadsheetApp.openById(ssId).getSheetByName("Programa");
   var dades = sh.getDataRange().getValues();  

  var frase = "<b> Programa III Jornades </b> \n\n";

  for(i in dades)
  {
    var row = dades[i];
    var hora = row[0];
    var franja = row[1];
    var taller = row[2];
    var programa = row[3];
    var formador = row[4];

    frase = frase + hora + " " ;  
    frase = frase + franja + " " ;
    frase = frase + "/" + taller + " " ;
    frase = frase + "<i>" + programa + "</i>  ";
    frase = frase + formador + "\n \n ";


  }

  sendText(id_usuari,escriu_frase(frase,idioma));
  return true ;

}


//-------------------------------- Identificar usuari per email i posar ID de Telegram
 function iden(id,text,idioma)
{

  var sh = SpreadsheetApp.openById(ssId).getSheetByName("Participants");
  var dades = sh.getDataRange().getValues();  

  var trobat = 0;


  for(i in dades)
  {

    var row = dades[i];
    var num_usuari = row[12];

    if(num_usuari == id)
    {

      sendText(id,escriu_frase("Ja estas validat",idioma));
      return true;

    }

  }

 var em = text.split(" ");
 var email = em[1];  


  if(email === undefined)  
  {
   sendText(id,escriu_frase("Per validar-te escriu /iden seguit del teu email",idioma));
   return true;
   }

  for(i in dades)
  {
    var row = dades[i];
    var correu = row[5];
    var nom = row[1]

    if(correu == email)
    {

       var posicio= +i+1;
       sh.getRange(posicio,13).setValue(id);  
       sh.getRange(posicio,14).setValue(new Date());  

       var frase = escriu_frase("Moltes gracies, ",idioma)+ " " + nom +"."+ escriu_frase("Estas registrat i identificat a les III Jornades. Pots veure els teus /tallers ",idioma);
       sendText(id,frase);

      return true;

    }

  }

   sendText(id,escriu_frase("No t'he pogut registrar, Revisa el teu email",idioma));
   return true;          

}


// ----------------------------------------Crear QR amb id de telegram
function qr(id_usuari,idioma)
{


        var  caption = "Entrada a les III Jornades";

        var url_script = webAppUrl +"?accio=validar%26id=" + id_usuari ;
        var qr = "https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=" + url_script;


       var response = UrlFetchApp.fetch(qr);
       var binaryData = response.getContent();


         var blob = Utilities.newBlob(binaryData, 'image/jpeg', 'QR');
         sendPhoto3(id_usuari,blob, caption);

        return true;                      

}


function menu_idioma(id,idioma)
{

 var llista = new Array(
   [{"text": escriu_frase("Castellà",idioma), "callback_data": "/idi es"},
    {'text': escriu_frase("Català",idioma), "callback_data" : "/idi ca"},
    {"text": escriu_frase("Basc",idioma),"callback_data" : "/idi eu"}],
   [{"text": escriu_frase("Anglès",idioma),"callback_data" : "/idi en"},
    {"text": escriu_frase("Francés",idioma),"callback_data" : "/idi fr"},
    {"text": "Gallego","callback_data" : "/idi gl"}],
   [{"text": escriu_frase("Àrab",idioma), "callback_data" : "/idi ar"},
    {"text": "Tancar","callback_data" : "/tancar"}]) ;

  var tecles =  { inline_keyboard: llista    , resize_keyboard: true,one_time_keyboard : true  };

  sendText(id,escriu_frase("Selecciona idioma",idioma),tecles );  

  return true;

}

function test()
{
canvia_idioma("379302885","/idi ca");
}

//-------------------------------------------------Buscar amb ID de Telegram i posar idioma al full de càlcul
function canvia_idioma(id_usuari,text)
{

  var sh = SpreadsheetApp.openById(ssId).getSheetByName("Participants");
  var dades = sh.getDataRange().getValues();  


  var entrada = text.split(' ');  // Separa les paraules entrades en una matriu/array per tenir per una banda la comanda i d'altre els valors
  var idioma = entrada[1];

  var trobat = 0;


  for(i in dades)
  {
    var row = dades[i];
    var num_usuari = row[12];
    var lang = row[14];
    if(num_usuari == id_usuari)
    {
      var posicio= +i+1;
      sh.getRange(posicio,15).setValue(idioma);
      sendText(id_usuari,escriu_frase("El nou idioma ara és : ", idioma) );
      sendText(id_usuari,idioma);
    }
}

  return true;

}

// -----------------------------------Llegir idioma actual del full de càlcul a partir ID Telegram
function assigna_idioma(id,lang)
{

  var sh = SpreadsheetApp.openById(ssId).getSheetByName("Participants");
  var dades = sh.getDataRange().getValues();  

  for(i in dades)
  {
    var row = dades[i];
    var num_usuari = row[12];
    var idioma = row[14];
    if(num_usuari == id)
    {
      return idioma;
    }

  }

  return lang ;

}


// -------------------------------------Funcions per confirmar formulari i generar certificats


// Funció per a enviar missatge de confirmació a l'últim element del full de càlcul
function envia_mis()
{

var sh = SpreadsheetApp.openById(ssId) ; // identifica el full de càlcul
var sheet = sh.getSheets(); // Agafa totas els fulls
var dades = sheet[0].getDataRange().getValues(); // Agafa nomes el subfull segon de index 1

var ultim = sheet[0].getDataRange().getLastRow(); // Localitzem la posició de la última fila
var fila = dades[ultim-1]; // Carreguem la ultiam fila en una fila de treball

var timestamp = fila[0]; // Agafem la priemra columna d'index 0 que porta l'hora de gravació
var nom = fila[1]; // Agafem el nom de l'usuari a la columna 2 index 1
var cognoms = fila[2]; // Agafem els cognoms de l'usuari a la columna 3 index 2
var nif = fila[3]; // Agafem el nif de l'usuari a la columna 4 index 3
var mail = fila[5]; // Agafem el email del usuari a la columan 5 index 4
var franja1 = fila[9]; // Agafem el taller de la franja1  a la columna 9 index 8
var franja2 = fila[10]; // Agafem el taller de la franja2  a la columna 10 index 9


//var url_acces= url_script+"?accio=validar%26id=" + ultim ;
var url_acces= webAppUrl+"?accio=validar%26id=" + ultim ;  
var qr = "https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl="+ url_acces ;
 var img = UrlFetchApp
                         .fetch(qr)
                         .getBlob()
                       .setName("img");


var subject = "Inscripció a les Jornades " ;

var plain_email_body = "Gracies per la teva inscripció  " + nom +" !! ! \n\n " +
                       "Has formalitzat la teva inscripció a les III Jornades \n" +
                       "Els tallers que t'has inscrits són :  \n " +
                       "   Franja 1 : " + franja1 + "\n" +
                       "   Franja 2 : " + franja2 + "\n\n\n" +
                       "Aquest codiQR que t'adjuntem l'hauras de mostrar a l'entradada de les jornadas i als Tellers per validar la teva presencia";

var html_body =       "Gracies per la teva inscripció  " + nom +" <br /><br />" +
                       "Has formalitzat la teva inscripció a les III Jornades <br />" +
                       "Els tallers que t'has inscrits són :  <br />" +
                       "   Franja 1 : " + franja1 + "<br/>" +
                       "   Franja 2 : " + franja2 + "<br /><br /><br />" +
                       "Aquest codiQR que t'adjuntem l'hauras de mostrar a l'entradada de les jornadas i als Tellers per validar la teva presencia <br />";

var advancedOpts = { name: "Inscripció III Jornades", htmlBody: html_body ,body: img,attachments: img};

MailApp.sendEmail(mail, subject, plain_email_body, advancedOpts );

}

// ---------------------------------------------------Generació dels diplomes de forma automàtica

 function diploma()
{

  // full_Id = "1zhoiq_Y8ZBXCLjQHtqB8aHzNXbSJVCDX3wSuYmM";       // Id del full de càlcul
  full_Id = ssId;                                                // Id del full de càlcul
  //plantilla_Id = "1Q_wLvH0pm9CJHXAzObtLflSOj9J4cucoISDnr8";      // Id de la plantilla del certificat     
  //final_Id = "1OEjGJHcocqwU72029MYHifeVxbTBkT7ZLwys";           // Id de la copia de la plantilla

  var plantilla = DocumentApp.openById(plantilla_Id);   // Obrim la plantilla
  var final = DocumentApp.openById(final_Id);       // Obrim la copia de la plantilla
  var plantilla_paragrafs = plantilla.getBody().getParagraphs();   // Carreguem tots els paràgrafs de la Plantilla

  // Al full de càlcul, obrim el full (pestanya anomenat "Participants", per tant cal canviar el nom a la pestanya)
  var sh = SpreadsheetApp.openById(full_Id).getSheetByName("Participants");  // Accedim al full de càlcul
  var dades = sh.getRange(2,1,sh.getLastRow()-1,14).getValues();   // Llegim dels dades de la fila 2, columna 1, fins la ultima fila, columna 14


  final.getBody().clear();  // Esborrem la plantilla Final per començar de nou un certificat


// r[1], r[2] i  r[4] correspon a les posicions de columna de les dades que seran enviades i que son nom, cognoms i nif


  dades.forEach(function(r){  // funció que es va repetint per cada fila de dades
        combinat(r[1],r[2],r[4],plantilla_paragrafs,final );    // Envia les dades a la funció combinat per fer la subtitució
  })   // fi de la funcio forEach

}   // fi de la funció diploma


// ----------------------------------funció per a substitutir les posicions dels camps per les dades de la fila passada

function combinat(nom,cognoms,nif,plantilla_paragrafs,final)    //funció que fa la substitució
{
  plantilla_paragrafs.forEach(function(p){    // per cada paragraf

    final.getBody().appendParagraph(p               // Es situa en el document copia de plantilla
                                    .copy()
                                    .replaceText("{nom}",nom)   // Reemplaça el nom
                                    .replaceText("{cognoms}",cognoms) //Reemplaça els cognoms
                                    .replaceText("{nif}",nif) //Reemplaça nif
                                    );
  })   // fi de la funció forEach

   final.getBody().appendPageBreak();  // Si volem que els certificats estiguin junts en un unic document anem afegint pagines

}

~~~

- Anem al BotFather i copiem el token del nostre bot, que posarem a la variable global del App script:

![](imgs/02-xatbots-2020-3d524e6e.png)

![](imgs/02-xatbots-2020-e4781eb2.png)

![](imgs/02-xatbots-2020-4b46ccb6.png)

- Token del bot ptome: 1240462513:AAEx_Cr1qRBlmPMtNY9iz9FdDOGGz-7GJ20

- si volem consultar l'APi de Telegram: https://core.telegram.org/bots/api

- Per a depurar la comunicació amb el bot, cal fer servir el mètode post html. Podem fer-ho des del navegador web:

~~~
// https://api.telegram.org/botAPIToken/getUpdates

https://api.telegram.org/bot1240462513:AAEx_Cr1qRBlmPMtNY9iz9FdDOGGz-7GJ20/getUpdates
~~~

- Enviem un missatge "hola prova" dintre del bot. La resposta a la petició serà un JSON (refresquem al navegador):
~~~
{"ok":true,"result":[{"update_id":376179336,
"message":{"message_id":10,"from":{"id":379302885,"is_bot":false,"first_name":"Pau","last_name":"T","username":"Pausoft","language_code":"es"},"chat":{"id":379302885,"first_name":"Pau","last_name":"T","username":"Pausoft","type":"private"},"date":1591093404,"text":"hola prova"}}]}
~~~

- Ara assignem el app-script de Google al bot per processar les interaccions amb usuaris i poder consultar el full de càlcul des del bot, amb la funció de l'API "setWebHook". Li passem la URL de l'app web que obtenim en publicar el codi.
~~~
https://api.telegram.org/bot1240462513:AAEx_Cr1qRBlmPMtNY9iz9FdDOGGz-7GJ20/setWebHook?url=https://script.google.com/macros/s/AKfycbx-Mi2Pf2MErz3WO7WSgFby52bTRWxpVBT5INd7sgArhNtu7Rc6
~~~

- Com a resposta obtenim
~~~
{"ok":true,"result":true,"description":"Webhook was set"}
~~~

- A partir d'ara ja no podrem fer getUpdate amb el navegador. Hauríem d'esborrar el hook posant a la url una cadena buida. O millor:

~~~
https://api.telegram.org/bot1240462513:AAEx_Cr1qRBlmPMtNY9iz9FdDOGGz-7GJ20/deleteWebhook
~~~

- Si interactuem amb el bot, aquest ens respon amb el que hem configurat a l'script.

- Inici, el meu mòbil té idioma castellà. Ens identifiquem:

![](imgs/02-xatbots-2020-511eb16e.png)

- Canviar idioma

![](imgs/02-xatbots-2020-c4fffd82.png)

- Info Jornades i ajuda en català un cop canviat idioma

![](imgs/02-xatbots-2020-bf36f95c.png)

- Entrada:

![](imgs/02-xatbots-2020-bdf9905b.png)

## NOTES:

- Depuració:

1. Crear funcions que simulin la crida des de fora
  ~~~
  function testCertificat(){
    certificat("379302885","ca");
  }

  function testIden(){
    iden("379302885","","ca");
  }

  function testEscriure_frase(){
    escriu_frase("379302885","Prova");
  }
  ~~~

  - Depurar amb debugger: Picant a l'esquerra de l'editor de codi podem posar breakpoint
  - Escollir funció de test al desplegable i picant el "bitxo" activem depurador.

  ![](imgs/02-xatbots-2020-79f75c8c.png)

2. Enviar missatges de log quan passa alguna cosa. Després veure amb "mostra->registres". https://developers.google.com/apps-script/guides/logging#basic_logging
  ~~~
  Logger.log('Emailing data row ' + rowNumber + ' to ' + email);
  ~~~

3. Enviar missatges de depuració al bot:

~~~
id_debug=id_usuari;
...
function debug(text){
  sendText(id_debug,"DEBUG: "+ text);
}
~~~
