•?Miscellaneous
@S4ChannelsBot
@allstarwars_bot
@civicbot
@ForismaticBot
@QuotesRobot
@T3chnoBot
@Zodiac_bot


•??Video
@MovieS4bot ?
@CineNoticiasbot
@imdb ?
@iVideoBot
@MuviBot ?
@NetflixNewsBot
@Noticiasenbot
@SaveVideoBot
@TVSeriesRoBot
@vid ?
@YoutubeBestVideo
@YoutubeSearchBot
@YTbot
@ytWatchBot
@WiseplayBot


•??Text Formatting
@S4ChannelsBot
@bold ?
@fliptxtbot ?
@Formato_bot ?
@FormattingBot ?


•??Social
@FBvidzBot
@GHVipBot
@Instasave_bot
@RandTalkBot
@StrangerBot
@telewitterbot
@ThePicturesBot ?
@Tweetitbot ?
@twitch_chatbot
@vk_bot


•??Promotion
@StoreBot ?
@Tchannelsbot ?
@AlternativeStoreBot
@CanalesBot


•??Shopping
@alisearchbot ?
@amaznbot ?
@amazonESbot ?
@amazonglobalbot ?
@amazonpricebot ?
@AmazonSearchBot
@AirtimeBot
@azonbot ?
@btcprices_bot ?
@EbayGlobalBot ?
@My_Ali_Bot
@octopocket_bot
@octopocket_ES_bot
@shopper_bot ?
@ShurAmazonBot
@TrackAzonBot ???


•??Humour
@automemebot ?
@chuckbot ?
@cuanta_razon_bot
@cuanto_cabron_bot
@dogefy_bot
@gastabot
@Marfa_Bot
@Meneame_bot
@TalkBotBot
@vaya_GIF_bot
@visto_en_las_redes_bot
@xkcdinlinebot ?
@YT_auronplay_bot
@YT_elrubius_bot


•??Gaming
@cardgamesbot
@dicebot
@DuNord_Intel_Bot
@Fruitgamebot
@HangBot
@mau_mau_bot ?
@MyPokerBot
@oxbot
@PokedexProBot
@SteamStatus_Bot
@TelegamesBot
@TheConnect4Bot
@Triviabot
@wowrollbot
@RandomRedditBot
@country_bot


•?Bot creation
@chatfuelbot
@Manybot
@bigbotfactorybot
@Botfather
@TbotifyBot


•?⚒Sticker pack creation
@stickers
@HowToMakeStickersBot


•??Stickers & Gif’s
@clippy
@gif ?
@GifMaster_bot
@stckrbot
@tube2gif_bot


•??Photography
@allwallpaperbot
@automemebot ?
@bing ?
@CuteCatBot
@ImageSearchBot
@lovecatsbot
@NatGeoBot
@pic ?
@vintagepostersbot


•??Music
@DJSetsBot
@getmp3bot
@GetMusicBot
@IndustrialCoperaBot
@SerMusicoBot
@Spoti_bot
@Spotisbot ?
@Spotybot ?
@TechnoMusicBot
@vkm_bot ?
@vkmusic_bot ?


•?⚽️Sports
@barcelona_fc_bot
@EfeseBot
@MorataRobot
@NewsF1_bot ?
@soccer_goals_bot


•?☔️Weather
@aemetbot ?
@Eltiempobot
@the_weather_bot
@weathercast_bot
@Weatherman_bot


•??News
@BBCNewsBot
@CatalunyaNoticies_bot
@ElPais_bot
@Forbesbot
@Noticias24hBot
@MashableNewsBot
@SismosChileBot
@SkyNewsBot


•??Android & Tech News
@Andro4AllBot
@androidauthority_bot
@AndroideLibre_bot
@AndroidNoticiasBot
@AndroidNoticias_bot
@AppleNoticias_bot
@LinuxNoticiasbot
@MyAndroidNewsBot
@NoticiasGeekBot
@NoticiasMensajeria_Bot
@omicrono_ibot
@Str600bot
@TechCrunchBot
@TecnologiaNoticias_bot
@Timesnowbot
@TW_applesfera_bot
@TW_engadget_es_bot
@TW_gizmodoES_bot
@TW_telegram_bot
@TW_xataka_bot
@YT_nexxuzhd_bot


•? Apps & software
@APKbot
@apkdl_bot
@APKMirrors_Bot
@G0Bot ?
@wam_bot
@xxdamagemods_bot


•??Translation
@a8bot ? [Multi-lang]
@enhibot ? [English→Hindi]
@EnKnBot ? [English→Kannada]
@enmlbot ? [English→Malayalam]
@entabot ? [English→Tamil]
@entagbot ? [English→Filipino]
@langtranslatebot ? [Multi-Lang]
@MalayalamDictionBot ?
@Pronunciationbot [Multi-Lang]
@TrFaBot ? [English→Persian]
@YTranslatebot [Multi-Lang]


•??Group tools
@Botaniobot
@channelrushbot
@EmoteChatBot
@GroupButler_bot
@Jh0ker_welcomebot
@LogToolBot
@LSD_B0t
@MessageStatBot
@MessageStatisticsBot
@PinMsgBot
@Pollbot
@TagAlertBot
@TagRobot ?
@TheRulesBot
@treaderbot
@IchLoggeBot


•??Multipurpose
@Alfred_the_bot
@GO_Robot
@GypsyBot
@imandabot ?
@jamesthebot
@mokubot
@n8bot
@RadRetroRobot
@Utilsbot


•??Inline Web Search
@gflbot ?
@lmddgtfybot ?
@lmgtfy_bot ?


•?⚙Tools
@Alertbot
@BestTravelBot
@CalcuBot
@CloudFileBot
@Cocktailbot
@codeBeautify_bot ?
@cumpleanosbot
@davidtorrentbot
@delorean_bot
@DictionBot ?
@directionsbot
@drae_bot
@DrWebBot
@eeemobot ?
@EMTMADbot
@erowid_bot
@EuroMillionsBot
@ExchangeRatesBot
@feed_reader_bot
@getidsbot ?
@GetMyIDBot
@GetMyUserIdBot
@githubbot
@grocerylistbot
@job_bot
@jsi_telebot
@instantsoundbot
@LatexBot
@myid_bot ?
@OnePlusOneUtilityBot
@OPObot
@OSMbot
@QRBot
@QRCodeRoBot
@qrcrbot
@RssyBot
@Rachel_bot
@raebot ?
@rate_convert_bot
@returnjsonbot
@rfilesbot
@s2tbot
@Safe_bot
@SaveVideoBot
@simple_downloader_bot
@smtpbot
@spammerstlgrm_bot
@SpoilingBot ?
@Tgfiles_Bot
@TheRSSBot
@topdf_bot
@trabberbot
@TriggerResponseBot
@TZbot
@UnitConversionBot
@Unit_Converter_Bot
@UploadBot
@userinfobot
@utubebot
@WebpageBot
@wiki ?
@wolframbot
@YourMDbot ?

Send your Bots to @TheListBot
Via @botlist

•⚙ Utilities
Store Bot – @storebot
Bot Father – @botfather
Bots4Telegram Bot – @B4Tbot
Wolfram Bot – @wolframbot
Poll Bot – @pollbot
Utils Bot – @utilsbot
Translate Bot – @translate_bot
GitHub Bot – @githubbot
Tfiles Bot – @tfilesbot
Ticker BitCoin Bot – @tickerbtc_bot
Botler Bot – @jamesthebot
Files Bot – @filesbot
Latex Bot – @latexbot
ReadMe Bot – @readmebot
InstaSave Bot – @instasave_bot
Telegram Groups Bot – @telegroupsbot
Text to Speech Bot – @speechbot
GetMusic Bot – @getmusicbot
isUP Bot — @isup_bot
Calcu Bot — @calcubot
Paque Bot Maker – @paquebot
Nomad Bot (Taylor) – @nomadbot
SteamSale Bot – @steamsalebot
UnitConverter Bot – @unit_converter_bot
Gypsy Bot – @gypsybot
YANDEX Translator Bot – @ytranslatebot
Translator Bot – @tlbot
RollGram RPG Dice Bot – @rollgrambot
OpenStreetMap – @osmbot
Life Quote Bot – @life_quotes_bot
Grocery List Bot – @grocerylistbot
Getgems Bot (Julia) – @getgemsbot
Trabber Bot – @TrabberBot
Alert Bot – @alertbot
Pronunciation Bot – @pronunciationbot
Espeak Bot – @espeakbot
Amazon Bot – @azonbot

iMobile Bot (Iran) – @imobilebot
Otaghe8 Bot (Iran) – @Otaghe8Bot
PlayStore Bot (Iran) – @playstorebot
AgahiGram Bot (Iran) – @AgahiGramBot

Tipo de Cambio CRC Bot (Spanish) – @tipodecambiobot
Podcast Bot (Spanish) – @podcast_bot
Precio Bot (Spanish) – @preciobot

Prezzo Bot (Italy) – @prezzobot
Cucù Bot (Italy) – @cucubot
Track Bot (Italy) – @trackbot
TrainItaly Bot (Italy) – @trainitalybot


•? Entertainment
RateSticker Bot – @ratestickerbot
Chuck Bot – @chuckbot
Movie Bot – @movieS4Bot
Botler Bot – @jamesthebot
Dice Bot – @dicebot
Secret Bot – @secretbot
MagicConchShell Bot – @magicconchshellbot
Quote Of The Day Bot – @qotdbot
Rate Me Now! Bot – @ratemenowbot
Forever Alone Bot – @ForeverAlone_bot
Funny Clip Bot – @funnyclip_bot
Word of the Day Bot – @wotdbot
The Number Bot – @thenumberbot
Should I Bot – @shouldibot
Funny Images Every Day Bot – @funnyimagesbot
Daily Music Bot – @best_music_bot

Emad Khan Bot (Iran) – @emadbot
Ramona Bot (Iran) – @ramonabot
MP Bot (Iran) – @mpbot
Manooch Bot (Iran) – @mrghaatbot

Matteo Salvini Bot (Italy) – @matteosalvini_bot
Almanacco Bot (Italy) – @almanaccobot
Italia Bot (Italy) – @italia_bot


•? Games
Hang Bot – @hangbot
Black Jack Bot – @blackjackbot
Trivia Bot – @triviabot
Sudoku Bot – @sudokubot
Game Bot – @kidbot
SteamSale Bot – @steamsalebot
OnTheHouse Bot – @oth_bot
Game Akinator Bot – @myakinator_bot
Would You Rather…? Bot – @ratherbot
Ingress Map Bot – @ingressmap_bot
Dungeon Bot – @dungeoncrawlerbot
ChessMaster Bot – @chessmasterbot
My Poker Bot – @MyPokerBot

Joko Bot (Iran) – @jokobot
XO Game Bot (Iran)- @oxbot

BukToPuHa Bot (Russia)- @buktopuhabot

Spaco Bot (Italy) – @spacobot


•? Education
Grammar Bot – @grammarbot
Dictionary Bot – @dictionbot
Translate Bot – @translate_bot
Wolfram Alpha Query Bot – @wolframalphaquerybot
ILI Classroom Bot – @ILI_bot
Tefl Bot – @teflbot

Novler Bot (Iran) – @novlerbot
Vajehyab Bot (Iran) – @vajehyabbot
English Cafe Bot (Iran) – @englishcafe_bot


•? Photo & Video
YouTube Search Bot – @youtubesearchbot
Image Bot – @imagebot
Movie Bot – @movieS4Bot
iFilm Bot – @movieS4Bot
InstaGet Bot – @instagetbot
ImageSearch Bot – @imagesearchbot
Gif Bot – @gifgifbot
Just Video Bot – @justvideobot
Designers Tools Bot – @designersbot
Car Show Bot – @car_show_bot
Cats Bot – @catcat_bot
Save Video Bot – @savevideobot


•? Social
Stranger Bot – @strangerbot
Job Seeker Bot – @job_bot
All New Stickers Bot – @AllStickerBot
Tweeting Bot – @tweetingbot
Rate Me Now! – @ratemenowbot
Hotorbot – @hotorbot
Taylor Swift fan Bot – @tay_bot
MatchMaker Bot – @matchmaker_bot

VK Bot (Russia)- @vk_bot
iTaxi Chisinau Bot (Russia) – @itaxibot

Quelli Che Il Panda Bot (Italy) – @quellicheilpandabot


•? News
Bots4Telegram Bot – @B4Tbot
Rub Bot – @rubbot
Ticker BitCoin Bot – @tickerbtc_bot
Ludo Bot – @ludobot
ReadMe Bot – @readmebot
Engadget Bot – @engadgetbot
ExchangeRates Bot – @exchangeratesbot
Netflix Bot – @netflixnewsbot
App of the Day Bot – @appofthedaybot

IYTclub Bot (Iran) – @iytclub_robot
APCA Bot (Iran) – @apcabot
SellGame Bot (Iran) – @sellgamebot
FC Bayern News Bot (Iran) – @fcbmbot

SPB Stop Bot (Russia)- @spbstopsbot

Noticias en Serie Bot (Spanish) – @noticiasenbot

La Formica Bot (Italy) – @laformicabot
Destiny News Bot (Italy) – @destinynews_bot

Dakwah Bot (Indonesia) – @dakwahbot


•? Weather
Weather Bot – @weatherbot
GetWeather Bot – @getweatherbot
Weathercast Bot – @weathercast_bot
Global Weather Bot – @globalweatherbot


•? Health
iCliniq Medical Advice Bot – @icliniqbot


•? Developers
Bot Father – @botfather
Paque Bot Maker – @paquebot
Tbotify Bot – @TbotifyBot
Botan Web – Botan Web
Store Bot Community – Store Bot Community
TeleSeed Bot – @teleseed


•?? Iran
iMobile Bot, Utilities – @imobilebot
Otaghe8 Bot, Utilities – @Otaghe8Bot
PlayStore Bot, Utilities – @playstorebot
AgahiGram Bot, Utilities – @AgahiGramBot

Emad Khan Bot, Entertainment – @emadbot
Ramona Bot, Entertainment – @ramonabot
MP Bot, Entertainment – @mpbot
Manooch Bot, Entertainment – @mrghaatbot

Joko Bot, Games – @jokobot
XO Game Bot, Games- @oxbot

Vajehyab Bot, Education – @vajehyabbot
Novler Bot, Education – @novlerbot
English Cafe Bot, Education – @englishcafe_bot

APCA Bot, News – @apcabot
IYTclub Bot, News – @iytclub_robot
SellGame Bot, News – @sellgamebot
FC Bayern News Bot, News – @fcbmbot


•?? Russia
BukToPuHa Bot, Games – @buktopuhabot

VK Bot, Social – @vk_bot
iTaxi Chisinau Bot, Social – @itaxibot

SPB Stop Bot, News – @spbstopsbot


•?? Spain
Tipo de Cambio CRC Bot, Utilities – @tipodecambiobot
Podcast Bot, Utilities – @podcast_bot
Precio Bot, Utilities- @preciobot

Noticias en Serie Bot, News – @noticiasenbot


•?? Italy
Prezzo Bot, Utilities – @prezzobot
Cucù Bot, Utilities – @cucubot
Track Bot, Utilities – @trackbot
TrainItaly Bot, Utilities – @trainitalybot

Matteo Salvini Bot, Entertainment – @matteosalvini_bot
Almanacco Bot, Entertainment – @almanaccobot
Italia Bot, Entertainment – @italia_bot

Spaco Bot, Games – @spacobot

Quelli Che II Panda Bot, Social – @quellicheilpandabot

La Formica Bot, News – @laformicabot
Destiny News Bot, News – @destinynews_bot
